from __future__ import annotations
from pathlib import Path
from xml.etree import ElementTree as ET
import sys


XML_NS = {"xsi": "http://www.w3.org/2001/XMLSchema-instance"}
XSD_NS = {"xs": "http://www.w3.org/2001/XMLSchema"}


def get_schema(xml_file: Path) -> Path:
    xml_document = ET.parse(xml_file)
    schema_relative_path: str = xml_document.getroot().attrib[
        str(ET.QName(XML_NS["xsi"], "noNamespaceSchemaLocation"))
    ]
    return xml_file.parent / schema_relative_path


if __name__ == '__main__':
    xml_path = Path(sys.argv[1])
    assert xml_path.exists()
    schema_path = get_schema(xml_path)
    assert schema_path.exists()
    print(schema_path)
