'''
Najde a vypise plaintexty (i.e. specialni)
'''
import xml.etree.ElementTree as ET
import xml.dom.minidom as dom
from pathlib import Path

data = Path('public', 'data')

result: list[tuple[str, ET.Element]] = []

for strom in data.glob('strom-*.xml'):
    doc = ET.parse(strom)
    strom_id = doc.getroot().attrib['id']
    for technika in doc.findall('./technika'):
        jmeno: str = technika.find('./jméno').text # type: ignore
        for efekt in technika.findall('./*'):
            if efekt.find('./speciální') is not None:
                result.append((f'[{strom_id}] {jmeno}', efekt))

for jmeno, efekt in result:
    print()
    print(jmeno)
    pretty_xml = dom.parseString(ET.tostring(efekt, encoding='utf-8')).toprettyxml()
    print('\n'.join(line for line in pretty_xml.splitlines()[1:] if line.strip()))

print()
print('Pocet nalezenych technik:', len(result))
