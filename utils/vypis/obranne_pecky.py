'''
Najde a vypise obranne efekty, ktere davaji zasah utocnikovi
'''
import xml.etree.ElementTree as ET
import xml.dom.minidom as dom
from pathlib import Path

data = Path('public', 'data')

DEFENSIVNI = 'úhyb', 'redukce'

result: list[tuple[str|None, ET.Element]] = []

for strom in data.glob('strom-*.xml'):
    doc = ET.parse(strom)
    for technika in doc.findall('./technika'):
        jmeno = technika.find('./jméno').text # type: ignore
        for efekt in technika.findall('./obranná'):
            for heslo in efekt.findall('./heslo'):
                if heslo.attrib['ref'] not in DEFENSIVNI:
                    result.append((jmeno, efekt))

for jmeno, efekt in result:
    print()
    print(jmeno)
    pretty_xml = dom.parseString(ET.tostring(efekt, encoding='utf-8')).toprettyxml()
    print('\n'.join(line for line in pretty_xml.splitlines()[1:] if line.strip()))

print()
print('Pocet nalezenych technik:', len(result))
