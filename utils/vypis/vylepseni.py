'''
Najde a vypise nelinearni vylepseni
'''
import xml.etree.ElementTree as ET
import xml.dom.minidom as dom
from pathlib import Path
from collections import defaultdict

data = Path('public', 'data')

mozne_vylepseni: dict[str, set[str]] = defaultdict(set)
kolik_technik_vylepsuju: dict[str, set[str]] = defaultdict(set)

for strom in data.glob('strom-*.xml'):
    doc = ET.parse(strom)
    for technika in doc.findall('./technika'):
        id: str = technika.attrib['id']
        jmeno: str = technika.find('./jméno').text # type: ignore
        for target in technika.findall('.//vylepšuje-techniku'):
            target_id = target.attrib['ref']
            mozne_vylepseni[target_id].add(id)
            kolik_technik_vylepsuju[id].add(target_id)


print('\nTechniky ktere maji vice moznosti jak se vylepsit:')
for tech, vylepseni in mozne_vylepseni.items():
    if len(vylepseni) > 1:
        print(' ', tech, ' -> ', *vylepseni)

print('\nVylepseni ktera vylepsuji vice technik zaraz:')
for vyl, techniky in kolik_technik_vylepsuju.items():
    if len(techniky) > 1:
        print(' ', vyl, ' <- ', *techniky)
