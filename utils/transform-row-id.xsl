<?xml version="1.0"?>

<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:strip-space elements="*" />
    <xsl:output method="text" encoding="utf-8" indent="no" />
    <xsl:template match="* | node()| @*">
        <xsl:copy>
            <xsl:apply-templates select="@*|*|node()" />
        </xsl:copy>
    </xsl:template>

    <xsl:template match="@dbg-row-id" />
</xsl:stylesheet>