'''
Generuje public/generated/prehled-stromu.xml
'''
from __future__ import annotations
import xml.dom.minidom as dom
from xml.dom.minidom import parse
from pathlib import Path

DOM = dom.getDOMImplementation()
assert DOM


def extract_short_strom(strom_def: Path) -> dom.Element:
    document = parse(strom_def.open())

    root: dom.Element = document.documentElement
    root.removeAttribute('xmlns:xsi')
    root.removeAttribute('xsi:noNamespaceSchemaLocation')
    for child in root.getElementsByTagName('technika'):
        if child.parentNode is root:
            root.removeChild(child)
            child.unlink()

    root.normalize()
    for chn in root.childNodes:
        if isinstance(chn, dom.Text):
            chn.data = str(chn.data).strip()

    return root


document = DOM.createDocument(None, "přehled-stromů", None)
document_root: dom.Element = document.documentElement
document_root.appendChild(document.createComment(
    " Soubor je generovan skriptem utils/generate_prehled_stromu.py "
))

OUTPUT_PATH = Path('public/generated/prehled-stromu.xml')
OUTPUT_PATH.parent.mkdir(exist_ok=True)

for strom_path in Path('public/data').glob('strom-*.xml'):
    document_root.appendChild(extract_short_strom(strom_path))

xml_string = document.toprettyxml(encoding='UTF-8')
with open(OUTPUT_PATH, 'wb') as document_file:
    document_file.write(xml_string)
