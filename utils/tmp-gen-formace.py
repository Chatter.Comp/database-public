from __future__ import annotations
import collections
import enum
from functools import reduce
import operator
from random import choice
import random
import typing

DEBUG = True


class Heslo(enum.Enum):
    Odhozeni = 'Odhození'
    Zmateni = 'Zmatení'
    Roztristeni = 'Roztříštění'
    Ochromeni = 'Ochromení'
    Paralyza = 'Paralýza'
    DMG = 'Zranění'
    DMG_ = 'DMG_'


HESLA = list(Heslo)

HODNOTA_HESLO = {
    Heslo.Ochromeni: 3,
    Heslo.Odhozeni: 1,
    Heslo.Roztristeni: 4,
    Heslo.Paralyza: 3,
    Heslo.Zmateni: 2,
    Heslo.DMG: 2,
    Heslo.DMG_: 2,
}

class TypFormace(typing.NamedTuple):
    name: str
    celkova_hodnota: int
    rozptyl_hodnoty: int
    pocet_formaci: int


class Formace:
    def __init__(self) -> None:
        self._data: collections.Counter[Heslo] = collections.Counter()

    def __add__(self, other: Formace) -> Formace:
        new = Formace()
        new._data.update(self._data.elements())
        new._data.update(other._data.elements())
        return new

    def add(self, heslo: Heslo) -> None:
        self._data[heslo] += 1

    @property
    def cena(self) -> int:
        cena = 0
        for heslo, pocet in self._data.items():
            cena += HODNOTA_HESLO[heslo] * pocet
        return cena
    
    def distance(self, other: Formace) -> int:
        distance = 0
        for heslo in Heslo:
            distance += abs(self._data[heslo] - other._data[heslo])
        return distance
    
    def __str__(self) -> str:
        data = { 
            **self._data,
            Heslo.DMG_: 0,
            Heslo.DMG: self._data[Heslo.DMG] + self._data[Heslo.DMG_],
        }
        add = f' ({self.cena})' if DEBUG else ''
        return ', '.join(
            f'{pocet}{"x" if heslo != Heslo.DMG else ""} {heslo.value}'
            for heslo, pocet 
            in sorted(sorted(data.items(), key=lambda x: x[0].value),
                      key = lambda x: x[1],
                      reverse=True)
            if pocet > 0
        ) + add

def is_different(f: Formace, others: list[Formace]) -> bool:
    for o in others:
        diff = f.distance(o)
        if diff >= 3:
            continue
        return False
    return True

def generate(typ: TypFormace) -> list[Formace]:
    result: list[Formace] = []
    while len(result) < typ.pocet_formaci:
        candidate = Formace()
        while candidate.cena < typ.celkova_hodnota - typ.rozptyl_hodnoty:
            candidate.add(random.choice(HESLA))
        if candidate.cena > typ.celkova_hodnota:
            continue
        if not is_different(candidate, result):
            continue
        result.append(candidate)
    return result

def main() -> None:
    typy_formaci = [
        TypFormace("Jednoduche (2 bod)", celkova_hodnota=10, rozptyl_hodnoty=1, pocet_formaci=10),
        TypFormace("Stredni (3 body)", celkova_hodnota=14, rozptyl_hodnoty=2, pocet_formaci=10),
        TypFormace("Slozite (4 body)", celkova_hodnota=18, rozptyl_hodnoty=3, pocet_formaci=10),
    ]
    for typ in typy_formaci:
        while True:
            seznam = generate(typ)
            total = reduce(lambda x, y: x + y, seznam)
            if abs(total._data.most_common()[0][1] - total._data.most_common()[-1][1]) > 1:
                continue
            print(typ.name)
            print(*seznam, sep='\n')
            if DEBUG:
                print('TOTAL', total)
            print()
            break

if __name__ == '__main__':
    main()
