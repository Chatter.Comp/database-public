set -e

for xml_path in public/data/*.xml
do
    xsd_path=`python3 utils/get_schema.py "$xml_path"`
    xmllint --nowarning --noout --schema "$xsd_path" "$xml_path"
done
