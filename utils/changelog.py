'''
Prints changelog to stdout.

Usage:
  python3 utils/changelog.py > public/tmp-changelog/$(date -I).diff

And then view in browser at e.g.:
  /tmp-changelog/index.html?file=2024-07-16.diff
'''
import subprocess

IGNORE_PREFIX = ('diff ', 'index ', '--- a', '@@ ')
FILENAME_PREFIX = '+++ b'

def get_diff():
    with subprocess.Popen(['git', 'diff', '-U9999', '--no-color'], stdout=subprocess.PIPE, encoding='utf-8') as res:
        if not res.stdout:
            return
        for line in res.stdout:
            if line.startswith(IGNORE_PREFIX):
                continue
            if line.startswith(FILENAME_PREFIX):
                file_path = line[len(FILENAME_PREFIX):]
                assert file_path.startswith('/')
                yield file_path
                continue
            assert line.startswith(('+', '-', ' '))
            yield line


INDENT_TRESHOLD_BY_FILE = (  # omitted files are ignored
    ('/public/data/strom-', 4),  # uses .startswith
    ('/public/data/definice.xml', 8),
)
def get_treshold(filename: str) -> int | None:
    for filename_prefix, indent in INDENT_TRESHOLD_BY_FILE:
        if filename.startswith(filename_prefix):
            return indent
    return None

def get_indentation(line: str) -> int:
    return len(line) - len(line.lstrip())


out: dict[str, list[str]] = {}  # filename -> lines
this_file_lines: list[str] | None = None
buffer: list[str] = []
is_buffer_dirty = False
indent_treshold: int | None = None

def flush():
    global is_buffer_dirty
    global buffer
    assert this_file_lines is not None
    if is_buffer_dirty:
        this_file_lines.extend(buffer)
    buffer = []
    is_buffer_dirty = False


for line in get_diff():
    if not line[1:].strip():
        continue
    prefix = line[0]
    if prefix == '/':
        this_file_lines = []
        out[line.rstrip()] = this_file_lines
        indent_treshold = get_treshold(line)
        continue
    assert this_file_lines is not None
    if indent_treshold is None:
        continue
    indent = get_indentation(line[1:])

    if indent < indent_treshold:
        flush()
        this_file_lines.append(line)

    elif indent == indent_treshold:
        if buffer:
            buffer.append(line)
            flush()
        else:
            buffer.append(line)
            if line[0] != ' ':
                is_buffer_dirty = True

    else:
        buffer.append(line)
        if line[0] != ' ':
            is_buffer_dirty = True


def count_by_prefix(items: list[str], prefixes: tuple[str, ...]) -> int:
    return sum(1 for line in items if line.startswith(prefixes))

for filename, lines in out.items():
    if not lines:
        continue
    a_count = count_by_prefix(lines, (' ', '-'))
    b_count = count_by_prefix(lines, (' ', '+'))
    print(f'diff --git a{filename} b{filename}')
    print(f'--- a{filename}')
    print(f'+++ b{filename}')
    print(f'@@ -1,{a_count} +1,{b_count} @@')
    for line in lines:
        print(line, end='')
