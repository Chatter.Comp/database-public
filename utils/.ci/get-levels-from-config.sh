RESPONSE=$(curl 'https://narutolarp.cz/profile/api-public-xml.php?subject=config')
LEVEL_NORMAL=$(echo "$RESPONSE" | sed -nr 's#.+<name>level_normal</name><value>([0-9]+)</value>.+#\1#p')
LEVEL_SENSEI=$(echo "$RESPONSE" | sed -nr 's#.+<name>level_sensei</name><value>([0-9]+)</value>.+#\1#p')
echo $LEVEL_NORMAL $LEVEL_SENSEI
