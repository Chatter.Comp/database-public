set -e

apk update
apk add curl

if [ -f elm ]
then
    echo "elm binary already exists"
else
    apk add curl
    echo "downloading elm..."
    curl -L -o elm.gz https://github.com/elm/compiler/releases/download/0.19.1/binary-for-linux-64-bit.gz
    gunzip elm.gz
    chmod +x elm
    ./elm --version
fi

if [ -d .elm-cache ]
then
    mkdir -p ~/.elm
    cp -r .elm-cache/* ~/.elm/
else
    mkdir .elm-cache
fi
