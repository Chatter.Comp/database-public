# Veřejná databáze narutolarpu

> Detailnější popis jak databáze funguje je na https://narutolarp.gitlab.io/database-public/info/

Jádrem databáze jsou XML soubory obsahující samotná data
* `public/data/strom-*.xml` - všechny technik ve hře, každý strom má vlastní soubor
* `public/data/definice.xml` - zásahy, hesla, etc.
* `public/data/specialni-schopnosti.xml` - všechny speciální schopnosti

Techniky je možné zobrazit na
* https://narutolarp.gitlab.io/techniky/skilltree.html
* https://narutolarp.gitlab.io/techniky/list.html
* https://narutolarp.gitlab.io/techniky/table.html

Zobrazování technik mají nastarosti jednotlivé Elm aplikace v `skilltree/src/App/`  
(tedy `skilltree/src/App/Skilltree.elm`, `skilltree/src/App/List.elm`, `skilltree/src/App/Table.elm`)


## Jak pracovat lokálně

https://wiki.python.org/moin/BeginnersGuide/Download 3.11 nebo novější

https://guide.elm-lang.org/install/elm

Terminál (shell) _(pro windowsáky např https://gitforwindows.org/#bash nebo WSL)_

### XML

`public/data/` - pro editaci XML stačí jakýkoli editor, který umí XSD schema.

XSD schema máme v `public/data/schemas/` - na ty většinou není potřeba sahat.

Některá schémata (`*-generated.xsd`) jsou generovaná skriptem `utils/generate_enums.py`,
ten je potřeba spouštět jen pokud se přidalo/odebralo nějaké id (např nová/smazaná technika).
(Tedy po naklonování repozitáře není potřeba spouštět.)  
Příkaz se spouští z rootu projektu, tedy příkazem:  
`python3 utils/generate_enums.py` (nebo `python utils/generate_enums.py`)

### Frontend (zobrazování technik)

> frontend používá https://guide.elm-lang.org

Pro vývoj je potřeba být uvnitř složky skilltree/  
Projekt je třeba zkompilovat pomocí shell skriptu src/build.sh
```sh
cd skilltree
sh build.sh
```
Což ve složce `public/` vygeneruje soubory `bundle-*.js` (kde `*` je `Skilltree`, `List` nebo `Table`).  
Poté spustit libovolný http server (pro statický obsah) ve složce `public/`
```sh
cd public
python3 -m http.server  # nebo python -m http.server
```
A v prohlížečí otevřít servírovanou adresu (a jeden z html souborů `skilltree.html`, `list.html` nebo `table.html`)


------

http://localhost:8000/skilltree.html#jutsu=&specialni=aburame-klan+anbu+astralni-projekce+bahenni-styl+bleskovy-plast+casticovy-styl+cerne-blesky+chakrovy-jil+chinoike-klan+daimyo-straz+demon-prison+demonicka-fletna+experimentalni-lecitelstvi+fuma-klan+hadi-senjutsu+hoshigaki-klan+hozuki-klan+hyuuga-klan+iburi-klan+isobu+juugo-klan+kaguya-klan+kamenne-senjutsu+kansai-klan+kokuo+kovarske-umeni+krystalicky-styl+kult-jashina+kurama-klan+lavovy-styl+ledovy-styl+lesni-styl+loutkar+magneticky-styl+medi-ninja+nara-klan+opila-pest+osm-bran+pavouci-techniky+pecetici-jednotka+pecetici-mnich+plameny-samadhi+plast-neviditelnosti+pokusny-subjekt+privolani-vrany+ricni-senjutsu+salamandr+samurai+senzor+sermir+shukaku+slimaci-senjutsu+snezny-styl+son-goku+spalujici-styl+styl-boure+styl-hromu+styl-pary+svetelny-styl+tajfunovy-styl+twin-demon-technique+uceni-uzushiokagure+uchiha-klan+uzumaki-klan+vetrna-jednotka+vybusny-styl+yamanaka-klan+yang-imitation+zabi-senjutsu&neschopnosti=&level=5&element=null
