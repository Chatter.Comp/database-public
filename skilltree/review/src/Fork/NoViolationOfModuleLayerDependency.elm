module Fork.NoViolationOfModuleLayerDependency exposing
    ( rule
    , ModuleLayer(..), ModuleLayerDependency(..)
    )

{-| If you defined a layer dependency rule like `A <- B`, elm-review-module-layer-dependency will detect an error with below importing.

    module A

    import B.Data

Module A depends on module B (`A -> B`) is a violation of defined rule.


# Rule

@docs rule


# Type

@docs ModuleLayer, ModuleLayerDependency

-}

import Dict exposing (Dict)
import Elm.Docs
import Elm.Syntax.Import exposing (Import)
import Elm.Syntax.Module as Module exposing (Module)
import Elm.Syntax.Node as Node exposing (Node)
import Review.Project.Dependency exposing (Dependency, modules)
import Review.Rule as Rule exposing (Error, Rule)


{-|


## Example configuration

    config : List Rule
    config =
        [ NoViolationOfModuleLayerDependency.rule moduleLayerRule
        ]

    moduleLayerRule : ModuleLayerDependency
    moduleLayerRule =
        ModuleLayerDependency
            [ infraLayer
            , applicationLayer
            , DefaultLayer
            , adapterLayer
            ]

    adapterLayer : ModuleLayer
    adapterLayer =
        ModuleLayer
            [ [ "Adapter" ]
            , [ "Main" ]
            ]

    applicationLayer : ModuleLayer
    applicationLayer =
        ModuleLayer
            [ [ "Application" ]
            ]

    infraLayer : ModuleLayer
    infraLayer =
        ModuleLayer
            [ [ "Infra" ]
            ]

-}
rule : ModuleLayerDependency -> Rule
rule layerRule =
    Rule.newModuleRuleSchema "NoViolationOfModuleLayerDependency" { moduleName = [], moduleLayerNumber = 0, externalModules = [] }
        |> Rule.withDirectDependenciesModuleVisitor dependenciesVisitor
        |> Rule.withModuleDefinitionVisitor (moduleDefinitionVisitor layerRule)
        |> Rule.withImportVisitor (importVisitor layerRule)
        |> Rule.fromModuleRuleSchema


dependenciesVisitor : Dict String Dependency -> Context -> Context
dependenciesVisitor deps ctx =
    let
        externalModules : List Elm.Docs.Module
        externalModules =
            Dict.values deps
                |> List.concatMap modules
    in
    { ctx | externalModules = List.map .name externalModules }


type alias Context =
    { moduleLayerNumber : Int
    , moduleName : ModuleName
    , externalModules : List String
    }


isExternal : Context -> ModuleName -> Bool
isExternal ctx name =
    ctx.externalModules
        |> List.filter ((==) (moduleNameAsString name))
        |> List.length
        |> (/=) 0


moduleDefinitionVisitor : ModuleLayerDependency -> Node Module -> Context -> ( List (Error {}), Context )
moduleDefinitionVisitor layerRule node ctx =
    ( []
    , { ctx
        | moduleLayerNumber =
            Node.value node
                |> Module.moduleName
                |> layerNumber ctx layerRule
                |> Tuple.first
        , moduleName = 
            Node.value node
                |> Module.moduleName
      }
    )


importVisitor : ModuleLayerDependency -> Node Import -> Context -> ( List (Error {}), Context )
importVisitor layerRule node ctx =
    let
        importingModuleName =
            Node.value node
                |> .moduleName
                |> Node.value
    in
    if isViolatedWithLayerRule layerRule ctx importingModuleName then
        ( [ Rule.error
                { message = "Found import to upper layer!"
                , details =
                    [ "This module is layer number " ++ String.fromInt ctx.moduleLayerNumber ++ "."
                    , "But the module is importing \"" ++ moduleNameAsString importingModuleName ++ "\" layer number " ++ (String.fromInt <| Tuple.first <| layerNumber ctx layerRule importingModuleName) ++ "."
                    ]
                }
                (Node.range node)
          ]
        , ctx
        )

    else
        ( []
        , ctx
        )


type alias ModuleName =
    List String


moduleNameAsString : ModuleName -> String
moduleNameAsString =
    List.intersperse "."
        >> List.foldl (\x acc -> acc ++ x) ""


{-| Provide module name to define rule.


## Example

A module `Aaa.Bbb.Ccc` define as ModuleLayer.
ModuleLayer
[ [ "Aaa", "Bbb", "Ccc" ]
]

-}
type ModuleLayer
    = ModuleLayer (List ModuleName)
    | ModuleLayerMutuallyExclusive (List ModuleName)
    | DefaultLayer


{-| Provide layer to define rule.


## Example

Define ModuleLayerDependency use `firstLayer: ModuleLayer` and `secondLayer: ModuleLayer`.
[ firstLayer
, secondLayer
]

It means dependency like `firstLayer <- secondLayer`.
If imported secondLayer module in firstLayer, elm-review will detect an error.

-}
type ModuleLayerDependency
    = ModuleLayerDependency (List ModuleLayer)


isViolatedWithLayerRule : ModuleLayerDependency -> Context -> List String -> Bool
isViolatedWithLayerRule moduleLayerRule ctx importingModuleName =
    let 
        ( n , exclusive ) = layerNumber ctx moduleLayerRule importingModuleName
    in
    if exclusive && not (isPrefix ctx.moduleName importingModuleName) then
        ctx.moduleLayerNumber <= n

    else
        ctx.moduleLayerNumber < n


layerNumber : Context -> ModuleLayerDependency -> List String -> (Int, Bool)
layerNumber ctx (ModuleLayerDependency layers) moduleName =
    let
        fold ( index, layer ) acc =
            case acc of
                Just _ ->
                    acc

                Nothing ->
                    case isInLayer layer moduleName of
                        (True, exclusive) -> 
                            Just (index, exclusive)
                        (False, _) ->
                            Nothing
    in
    if isExternal ctx moduleName then
        (defaultLayerNumber layers, False)

    else
        layers
            |> List.indexedMap (\index layer -> ( index, layer ))
            |> List.foldl fold Nothing
            |> Maybe.withDefault (defaultLayerNumber layers, False)


defaultLayerNumber : List ModuleLayer -> Int
defaultLayerNumber layers =
    layers
        |> List.indexedMap (\i x -> ( i, x ))
        |> List.foldl
            (\( index, layer ) acc ->
                if layer == DefaultLayer then
                    Just index

                else
                    acc
            )
            Nothing
        |> Maybe.withDefault (List.length layers)


isInLayer : ModuleLayer -> List String -> (Bool, Bool)
isInLayer layer moduleName =
    case layer of
        ModuleLayer layerModuleNames ->
            (List.any (isMatchWith moduleName) layerModuleNames, False)

        DefaultLayer ->
            (False, False)

        ModuleLayerMutuallyExclusive layerModuleNames ->
            ( List.any (isMatchWith moduleName) layerModuleNames
            , True
            )


isMatchWith : ModuleName -> ModuleName -> Bool
isMatchWith names moduleName =
    let
        isMatchWith_ namesX namesY =
            case ( namesX, namesY ) of
                ( [], _ :: _ ) ->
                    False

                ( x :: xs, y :: ys ) ->
                    if x == y || x == "*" || y == "*" then
                        isMatchWith_ xs ys

                    else
                        False

                ( _ :: _, [] ) ->
                    False

                ( [], [] ) ->
                    True
    in
    isMatchWith_ names moduleName


isPrefix : ModuleName -> ModuleName -> Bool
isPrefix short long =
    case (short, long) of
        (a :: [], b :: []) ->
            String.startsWith a b

        (a :: ass, b::bs) ->
            a == b && isPrefix ass bs

        _ ->
            False
