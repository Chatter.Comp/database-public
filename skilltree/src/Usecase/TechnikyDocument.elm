module Usecase.TechnikyDocument exposing (IsReady(..), TechnikyDocument, TechnikyDocumentMsg, TechnikyDocumentState, fetchAll, initState, isReady, updateState)

import Api
import DatabaseCache exposing (DatabaseCache)
import Env.Env
import Env.Util.BuildVariables
import GUID exposing (GUID, GUIDDict)
import Jutsu exposing (Jutsu)
import QuirkMechanicky exposing (QuirkMechanicky)
import Task
import Tree exposing (Tree)
import Url.Builder
import Utils exposing (ListNotEmpty, listNotEmpty)
import Xml.Decode


type alias TechnikyDocument =
    { trees : ListNotEmpty Tree
    , jutsuById : GUIDDict GUID.Jutsu Jutsu
    , specSchopnosti : ListNotEmpty QuirkMechanicky
    , cache : DatabaseCache
    }


type IsReady
    = Ready TechnikyDocument
    | Loading ( Int, Int )
    | Error (List String)


isReady : TechnikyDocumentState -> IsReady
isReady state =
    case state of
        LoadingState payload ->
            if List.isEmpty payload.errors then
                Loading
                    ( currentLoadedCount payload
                    , totalCountToLoad
                    )

            else
                Error payload.errors

        ReadyState document ->
            Ready document


currentLoadedCount : LoadingStatePayload -> Int
currentLoadedCount state =
    List.length state.stromy
        + Utils.boolToInt state.definiceFinished
        + Utils.boolToInt (not (List.isEmpty state.quirky))


totalCountToLoad : Int
totalCountToLoad =
    List.length xmlTreeDocumentsToFetch + 2


allLoaded : LoadingStatePayload -> Bool
allLoaded state =
    currentLoadedCount state == totalCountToLoad


type alias LoadingStatePayload =
    { stromy : List Tree
    , jutsuById : GUIDDict GUID.Jutsu Jutsu
    , quirky : List QuirkMechanicky
    , cache : DatabaseCache
    , definiceFinished : Bool
    , errors : List String
    }


type TechnikyDocumentState
    = LoadingState LoadingStatePayload
    | ReadyState TechnikyDocument


initState : TechnikyDocumentState
initState =
    LoadingState
        { stromy = []
        , jutsuById = GUID.dictEmpty
        , quirky = []
        , cache = initCache
        , definiceFinished = False
        , errors = []
        }


type TechnikyDocumentMsg
    = MsgTree (Result String ( Tree, List Jutsu ))
    | MsgSpec (Result String (List QuirkMechanicky))
    | MsgDefinice (Result String DatabaseCache.MapCache)


updateState : TechnikyDocumentMsg -> TechnikyDocumentState -> TechnikyDocumentState
updateState msg state =
    case state of
        ReadyState _ ->
            state

        LoadingState payload ->
            let
                newPayload : LoadingStatePayload
                newPayload =
                    updateStatePayload msg payload
            in
            if allLoaded newPayload then
                finalizeState newPayload

            else
                LoadingState newPayload


finalizeState : LoadingStatePayload -> TechnikyDocumentState
finalizeState state =
    case ( listNotEmpty state.quirky, listNotEmpty state.stromy ) of
        ( Just specSchopnosti, Just stromy ) ->
            ReadyState
                { trees = Tree.sort stromy
                , jutsuById = state.jutsuById
                , specSchopnosti = specSchopnosti
                , cache = state.cache
                }

        _ ->
            LoadingState { state | errors = "Empty payload" :: state.errors }


updateStatePayload : TechnikyDocumentMsg -> LoadingStatePayload -> LoadingStatePayload
updateStatePayload msg state =
    case msg of
        MsgTree (Result.Err err) ->
            { state | errors = err :: state.errors }

        MsgTree (Result.Ok ( strom, techniky )) ->
            { state
                | stromy = strom :: state.stromy
                , jutsuById = updateTechnikyById techniky state.jutsuById
                , cache = DatabaseCache.registerIterHelper Tree.updateCache [ strom ] state.cache
            }

        MsgSpec (Result.Err err) ->
            { state | errors = err :: state.errors }

        MsgSpec (Result.Ok quirky) ->
            { state
                | quirky = quirky
                , cache = DatabaseCache.registerIterHelper QuirkMechanicky.updateCache quirky state.cache
            }

        MsgDefinice (Result.Err err) ->
            { state | errors = err :: state.errors }

        MsgDefinice (Result.Ok updateCache) ->
            { state
                | cache = updateCache state.cache
                , definiceFinished = True
            }


updateTechnikyById : List Jutsu -> GUIDDict GUID.Jutsu Jutsu -> GUIDDict GUID.Jutsu Jutsu
updateTechnikyById list dict =
    case list of
        technika :: rest ->
            updateTechnikyById rest (GUID.dictSet (Jutsu.id technika) technika dict)

        [] ->
            dict



--------------------------


definicePaths : List ( String, String, String -> Xml.Decode.Decoder DatabaseCache.MapCache )
definicePaths =
    [ ( "zásahy", "zásah", DatabaseCache.zasahDecoder )
    , ( "hesla", "ofenzivní", DatabaseCache.hesloDecoder { defensivni = False } )
    , ( "hesla", "defenzivní", DatabaseCache.hesloDecoder { defensivni = True } )
    , ( "atributy", "atribut", DatabaseCache.atributDecoder )
    , ( "ceny", "cena", DatabaseCache.commonDecoder )
    , ( "cooldowny", "cooldown", DatabaseCache.commonDecoder )
    , ( "typy-vnímání", "typ-vnímání", DatabaseCache.commonDecoder )
    , ( "klíčová-slova", "klíčové-slovo", DatabaseCache.commonDecoder )
    , ( "typy-efektu", "typ-efektu", DatabaseCache.commonDecoder )
    , ( "modifikátory-efektu", "modifikátor-efektu", DatabaseCache.commonDecoder )
    , ( "elementy", "element", DatabaseCache.elementDecoder )

    -- , ( "pečeť-definice", "pečeť" ) TODO <pečeť id="pes" obrázek="dog" />
    ]


fetchAll : Cmd TechnikyDocumentMsg
fetchAll =
    Task.attempt MsgDefinice fetchDefinice
        :: Task.attempt MsgSpec fetchSpecialniSchopnosti
        :: List.map (Task.attempt MsgTree << fetchTree) xmlTreeDocumentsToFetch
        |> Cmd.batch


xmlUrlWithCachePunch : List String -> String
xmlUrlWithCachePunch path =
    Url.Builder.custom
        Env.Env.environment.databasePublicRoot
        (Env.Env.environment.databasePublicPathBase
            ++ ("data" :: path)
        )
        [ Url.Builder.string "v" Env.Util.BuildVariables.version
        ]
        Nothing


fetchDefinice : Task.Task String DatabaseCache.MapCache
fetchDefinice =
    Api.fetch
        { url = xmlUrlWithCachePunch [ "definice.xml" ]
        , decoder = cacheDecoder definicePaths
        }


fetchSpecialniSchopnosti : Task.Task String (List QuirkMechanicky)
fetchSpecialniSchopnosti =
    Api.fetch
        { url = xmlUrlWithCachePunch [ "specialni-schopnosti.xml" ]
        , decoder = Xml.Decode.path [ "quirk-mechanicky" ] (Xml.Decode.list QuirkMechanicky.decoder)
        }


fetchTree : String -> Task.Task String ( Tree, List Jutsu )
fetchTree fileName =
    Api.fetch
        { url = xmlUrlWithCachePunch [ fileName ]
        , decoder = Tree.decoder Jutsu.decoder
        }


xmlTreeDocumentsToFetch : List String
xmlTreeDocumentsToFetch =
    [ "strom-Blesk-Raiton.xml"
    , "strom-Boj-beze-zbrane-Taijutsu.xml"
    , "strom-Formovani-chakry-Keitai.xml"
    , "strom-Genjutsu-yin-styl.xml"
    , "strom-Leceni-yang-styl.xml"
    , "strom-Ohen-Katon.xml"
    , "strom-Peceteni-Fuinjutsu.xml"
    , "strom-Priprava-Ningu.xml"
    , "strom-Uskocnost-Kakuran.xml"
    , "strom-Voda-Suiton.xml"
    , "strom-Vzduch-Fuuton.xml"
    , "strom-Zbranove-techniky-Bukijutsu.xml"
    , "strom-Zeme-Doton.xml"
    ]



-- CACHE


initCache : DatabaseCache
initCache =
    DatabaseCache.empty
        |> DatabaseCache.registerIterHelper updateCacheVillage_hardcoded GUID.const.villages


updateCacheVillage_hardcoded : GUID GUID.Village -> DatabaseCache -> DatabaseCache
updateCacheVillage_hardcoded id =
    DatabaseCache.registerCommon id
        { name = GUID.villageName_TODO id
        , details = ""
        , alias = Nothing
        , zobrazeni = Nothing
        }


cacheDecoder :
    List ( String, String, String -> Xml.Decode.Decoder DatabaseCache.MapCache )
    -> Xml.Decode.Decoder DatabaseCache.MapCache
cacheDecoder tagNames =
    case tagNames of
        ( tag, childTag, decoder ) :: rest ->
            Xml.Decode.with
                (Xml.Decode.path [ tag ] (Xml.Decode.single (decoder childTag)))
                (\updateCache -> Xml.Decode.map ((>>) updateCache) (cacheDecoder rest))

        [] ->
            Xml.Decode.succeed identity
