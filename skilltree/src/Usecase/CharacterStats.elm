module Usecase.CharacterStats exposing (viewCharacterStats)

import Character exposing (Character)
import Css
import DatabaseCache exposing (DatabaseCache)
import GUID exposing (GUID)
import Html.Styled as Html exposing (Html)
import Html.Styled.Attributes as Attrs
import Jutsu
import Tree exposing (Tree)
import UI.Definition
import Utils exposing (ListNotEmpty)


viewCharacterStats : DatabaseCache -> ListNotEmpty Tree -> Character -> Html msg
viewCharacterStats cache trees char =
    let
        totalAttrs : List ( GUID GUID.Atribut, Int )
        totalAttrs =
            Tree.allJutsuPicked char trees
                |> List.concatMap Jutsu.givenAttributes
                |> DatabaseCache.sumAttributesWithDefaults cache
    in
    Html.div
        []
        (viewCharacterJutsuCount char
            :: DatabaseCache.viewValues "" cache totalAttrs
        )


viewCharacterJutsuCount : Character -> Html masg
viewCharacterJutsuCount char =
    Html.div
        [ Attrs.css
            [ Css.displayFlex
            , Css.property "gap" "1rem"
            ]
        ]
        [ UI.Definition.stringLabeled "Zvoleno technik" (Just (String.fromInt (Character.zvolenoTechnik char)))
        , UI.Definition.stringLabeled "Zbývající body" (Just (String.fromInt (Character.zbyvajiciXP char)))
        ]
