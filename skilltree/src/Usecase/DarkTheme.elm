port module Usecase.DarkTheme exposing
    ( Model
    , Msg
    , flagsDecoder
    , update
    , view
    , withoutTheming
    )

import Html.Styled as Html exposing (Html)
import Json.Decode
import UI.Checkbox


port darkThemeEmitter : Bool -> Cmd msg



-- MODEL


type Model
    = Model (Maybe Bool)


withoutTheming : Model
withoutTheming =
    Model Nothing


flagsDecoder : Json.Decode.Decoder Model
flagsDecoder =
    Json.Decode.map Model
        (Json.Decode.maybe (Json.Decode.field "darkTheme" Json.Decode.bool))



--UPDATE


type Msg
    = Toggle Bool


update :
    (Msg -> msg)
    -> Msg
    -> { model | darkTheme : Model }
    -> ( { model | darkTheme : Model }, Cmd msg )
update msgWrap (Toggle darkThemeEnabled) model =
    ( { model | darkTheme = Model (Just darkThemeEnabled) }
    , darkThemeEmitter darkThemeEnabled
        |> Cmd.map msgWrap
    )



-- VIEW


view : Model -> Html Msg
view (Model theme) =
    case theme of
        Just darkThemeValue ->
            UI.Checkbox.view
                { label = "Tmavý režim"
                , onChange = Toggle
                , value = darkThemeValue
                , editability = UI.Checkbox.Editable
                }

        Nothing ->
            Html.text ""
