module JutsuEffectVylepseni exposing
    ( Group
    , Reducer
    , ResolutionOrder
    , decoder
    , decoderSingle
    , empty
    , expectEqual
    , map
    , merge
    , resolveGUIDDict
    , resolveGUIDSet
    , resolveReplace
    )

import Dict exposing (Dict)
import Expect
import GUID exposing (GUID, GUIDDict, GUIDSet)
import List.Extra
import Utils exposing (ListNotEmpty)
import Xml.Decode


{-| Prvni je zakladni technika, potom jeji prvni vylepseni, ..., posledni je aktualni technika
-}
type alias ResolutionOrder =
    ListNotEmpty (GUID GUID.Jutsu)


type Group a
    = Group (GUIDDict GUID.Jutsu a)


empty : Group a
empty =
    Group GUID.dictEmpty


map : (a -> b) -> Group a -> Group b
map fn (Group payload) =
    payload
        |> GUID.dictToItems
        |> List.map (Tuple.mapSecond fn)
        |> GUID.dictFromItems
        |> Group


merge : (a -> a -> a) -> Group a -> Group a -> Group a
merge fn (Group a) (Group b) =
    Group (GUID.dictMerge fn a b)



-- REDUCERS


type alias Reducer a =
    ResolutionOrder -> Group a -> { old : a, new : a, prev : a, merged : a }


{-| mergePayloads: left argument should overwrite the right one
-}
reduce : (a -> a -> a) -> (a -> a -> a) -> a -> Reducer a
reduce mergePayloads diff init ( technika, vylepseni ) (Group data) =
    let
        resolve : List (GUID GUID.Jutsu) -> a
        resolve jutsu =
            jutsu
                |> List.filterMap (\id_ -> GUID.dictGet id_ data)
                |> List.foldl mergePayloads init

        ( oldVylepseni, newVylepseni ) =
            case List.Extra.unconsLast vylepseni of
                Just ( last, body ) ->
                    ( body, [ last ] )

                Nothing ->
                    ( [], [] )

        prev : a
        prev =
            resolve (technika :: oldVylepseni)

        new : a
        new =
            resolve newVylepseni
    in
    { old =
        diff prev new
    , new =
        new
    , prev =
        prev
    , merged =
        resolve (technika :: vylepseni)
    }


resolveReplace : a -> Reducer a
resolveReplace init =
    reduce
        (\left _ -> left)
        (\left _ -> left)
        init


resolveGUIDDict : Reducer (GUIDDict t a)
resolveGUIDDict =
    reduce GUID.dictUnion GUID.dictDiff GUID.dictEmpty


resolveGUIDSet : Reducer (GUIDSet t)
resolveGUIDSet =
    reduce GUID.setUnion GUID.setDiff GUID.setEmpty



-- XML


decoderSingle : List String -> Xml.Decode.Decoder a -> GUID GUID.Jutsu -> Xml.Decode.Decoder (Group a)
decoderSingle path payloadDecoder rootJutsuId =
    Xml.Decode.path path (Xml.Decode.list (decoderPairs payloadDecoder rootJutsuId))
        |> Xml.Decode.map uniqueId
        |> Xml.Decode.map GUID.dictFromItems
        |> Xml.Decode.map Group


uniqueId : List ( id, a ) -> List ( id, a )
uniqueId items =
    List.Extra.gatherEqualsBy Tuple.first items
        |> List.map (\( first, _ ) -> ( first, first ))
        |> List.map (Tuple.mapBoth Tuple.first Tuple.second)


decoder : List String -> Xml.Decode.Decoder a -> GUID GUID.Jutsu -> Xml.Decode.Decoder (Group (List a))
decoder path payloadDecoder rootJutsuId =
    Xml.Decode.path path (Xml.Decode.list (decoderPairs payloadDecoder rootJutsuId))
        |> Xml.Decode.map groupById
        |> Xml.Decode.map GUID.dictFromItems
        |> Xml.Decode.map Group


groupById : List ( id, a ) -> List ( id, List a )
groupById items =
    List.Extra.gatherEqualsBy Tuple.first items
        |> List.map (\( first, rest ) -> ( first, first :: rest ))
        |> List.map (Tuple.mapBoth Tuple.first (List.map Tuple.second))


decoderPairs : Xml.Decode.Decoder a -> GUID GUID.Jutsu -> Xml.Decode.Decoder ( GUID GUID.Jutsu, a )
decoderPairs payloadDecoder rootJutsuId =
    Xml.Decode.map2 Tuple.pair
        (Xml.Decode.maybe GUID.vylepseniDecoder
            |> Xml.Decode.map (Maybe.withDefault rootJutsuId)
        )
        payloadDecoder



-- TEST


expectEqual : Dict String a -> Group a -> Expect.Expectation
expectEqual expect (Group real) =
    GUID.dictExpectEqual expect real
