module JutsuAccess exposing
    ( JutsuAccess
    , accessDecoder
    , accessDetail
    , accessSuffix
    , canAccess
    , isPublic
    , isQuirk
    )

import DatabaseCache exposing (DatabaseCache)
import GUID exposing (GUIDSet)
import Xml.Decode


type JutsuAccess
    = Public
    | Quirk (GUIDSet GUID.QuirkMechanicky)


canAccess : GUIDSet GUID.QuirkMechanicky -> JutsuAccess -> Bool
canAccess quirksMechanicke access =
    case access of
        Public ->
            True

        Quirk quirkIds ->
            GUID.setIntersect quirkIds quirksMechanicke
                |> GUID.setLength
                |> (/=) 0


isPublic : JutsuAccess -> Bool
isPublic access =
    case access of
        Public ->
            True

        _ ->
            False


isQuirk : JutsuAccess -> Bool
isQuirk access =
    case access of
        Quirk _ ->
            True

        _ ->
            False



-- XML


accessDecoder : Xml.Decode.Decoder JutsuAccess
accessDecoder =
    Xml.Decode.oneOf
        [ Xml.Decode.path [ "veřejná" ] (Xml.Decode.single <| Xml.Decode.succeed Public)
        , GUID.refListPathDecoderNonEmpty [ "speciální-schopnost" ] |> Xml.Decode.map (Quirk << GUID.setFromList)
        ]



-- VIEW / STRING


accessSuffix : JutsuAccess -> String
accessSuffix acc =
    if acc == Public then
        ""

    else
        "*"


accessDetail : DatabaseCache -> GUIDSet GUID.QuirkMechanicky -> JutsuAccess -> Maybe String
accessDetail cache relevantQuirks acc =
    case acc of
        Public ->
            Nothing

        Quirk quirkIds ->
            let
                onlySelectedQuirks : GUIDSet GUID.QuirkMechanicky
                onlySelectedQuirks =
                    GUID.setIntersect quirkIds relevantQuirks
            in
            (if GUID.setLength onlySelectedQuirks > 0 then
                onlySelectedQuirks

             else
                quirkIds
            )
                |> GUID.setToList
                |> List.filterMap (DatabaseCache.name cache)
                |> String.join ", "
                |> (++) "* Speciální schopnost: "
                |> Just
