module Api exposing (FetchOptions, fetch, send)

import Http
import Task exposing (Task)
import Xml.Decode


type alias FetchOptions response =
    { url : String
    , decoder : Xml.Decode.Decoder response
    }


fetch : FetchOptions response -> Task String response
fetch options =
    request
        { method = "GET"
        , url = options.url
        , body = Http.emptyBody
        , decoder = options.decoder
        }


send : FetchOptions response -> String -> Task String response
send options body =
    request
        { method = "PUT"
        , url = options.url
        , body = Http.stringBody "application/xml" body
        , decoder = options.decoder
        }


type alias RequestParams response =
    { method : String
    , url : String
    , body : Http.Body
    , decoder : Xml.Decode.Decoder response
    }


request : RequestParams response -> Task String response
request options =
    Http.task
        { method = options.method
        , headers = []
        , url = options.url
        , body = options.body
        , resolver =
            Http.stringResolver
                (handleResponse
                    >> Result.andThen (Xml.Decode.run options.decoder)
                )
        , timeout = Nothing
        }


handleResponse : Http.Response String -> Result String String
handleResponse response =
    case response of
        Http.BadUrl_ url ->
            Result.Err ("The URL " ++ url ++ " was invalid")

        Http.Timeout_ ->
            Result.Err "Unable to reach the server, try again"

        Http.NetworkError_ ->
            Result.Err "Unable to reach the server, check your network connection"

        Http.BadStatus_ metadata _ ->
            case metadata.statusCode of
                500 ->
                    Result.Err "The server had a problem, try again later"

                400 ->
                    Result.Err "Verify your information and try again"

                _ ->
                    Result.Err "Unknown error"

        Http.GoodStatus_ _ body ->
            Result.Ok body
