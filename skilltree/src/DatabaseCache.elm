module DatabaseCache exposing
    ( DatabaseCache
    , GetAllWithName
    , HesloPayload
    , JutsuPayload
    , MapCache
    , QuirkPayload
    , RegisterSpecific
    , SimpleListDecoderUntrusted
    , SingleDecoderUntrusted
    , StringValidate
    , TreePayload
    , ViewListBuilder
    , allowZakladniUtok
    , atributDecoder
    , buildBlock
    , buildInline
    , buildString
    , commonDecoder
    , commonPayload
    , details
    , elementDecoder
    , elementDecoderJsonUntrusted
    , elements
    , empty
    , hesloDecoder
    , isDefensive
    , isDefined
    , name
    , nameAlias
    , quirkPayload
    , registerCommon
    , registerIterHelper
    , registerJutsu
    , registerQuirk
    , registerTree
    , simpleJutsuListDecoderJsonUntrusted
    , simpleListDecoderJsonUntrustedSpecialni
    , simpleNeschopnostListDecoderJsonUntrusted
    , stringValidateSpecialniSchopnost
    , stringValidateTree
    , sumAttributesWithDefaults
    , treeNameFromJutsu
    , viewDefinition
    , viewDefinitionMinor
    , viewListBuilder
    , viewName
    , viewSingleValueAsBlock
    , viewSingleValueBuilder
    , viewValues
    , viewValuesAlias
    , viewValuesAliasVsechny
    , viewValuesVsechny
    , withAlias
    , withComment
    , withLabel
    , withShort
    , withSuffix
    , withSuffixManual
    , zasahDecoder
    )

import Css
import GUID exposing (GUID, GUIDDict, GUIDSet)
import Html.Styled as Html exposing (Html)
import Json.Decode
import List.Extra
import UI
import UI.Definition
import Utils exposing (Vsechny(..))
import Xml.Decode
import XmlParser


type DatabaseCache
    = DatabaseCache DatabaseCachePayload


type alias DatabaseCachePayload =
    { common : GUIDDict GUID.Common CommonPayload
    , heslo : GUIDDict GUID.Heslo HesloPayload
    , jutsu : GUIDDict GUID.Jutsu JutsuPayload
    , tree : GUIDDict GUID.Tree TreePayload
    , atribut : GUIDDict GUID.Atribut AtributPayload
    , zasah : GUIDDict GUID.Zasah ZasahPayload
    , element : GUIDDict GUID.Element ElementPayload
    , specialniSchopnost : GUIDDict GUID.QuirkMechanicky QuirkPayload
    , idConflicts : GUIDSet GUID.Common
    }


payload : DatabaseCache -> DatabaseCachePayload
payload (DatabaseCache cache) =
    cache


type alias CommonPayload =
    { name : String
    , details : String
    , alias : Maybe String
    , zobrazeni : Maybe (ZobrazeniParams -> Html Never)
    }


type alias ZobrazeniParams =
    { jmeno : Html Never
    , pocet : Int
    }


type alias HesloPayload =
    { defensivni : Bool
    }


type alias JutsuPayload =
    { level : Int
    , strom : GUID GUID.Tree
    }


type alias TreePayload =
    {}


type alias AtributPayload =
    { defaultValue : Int
    }


type alias ZasahPayload =
    { zakladniUtok : Bool
    }


type alias ElementPayload =
    {}


type alias QuirkPayload =
    { element : GUIDSet GUID.Element
    , kekkeiGenkai : Bool
    , spojujeStromy : GUIDSet GUID.Tree
    }


empty : DatabaseCache
empty =
    DatabaseCache
        { common = GUID.dictEmpty
        , heslo = GUID.dictEmpty
        , jutsu = GUID.dictEmpty
        , atribut = GUID.dictEmpty
        , zasah = GUID.dictEmpty
        , element = GUID.dictEmpty
        , tree = GUID.dictEmpty
        , specialniSchopnost = GUID.dictEmpty
        , idConflicts = GUID.setEmpty
        }


isDefined : DatabaseCache -> GUID t -> Bool
isDefined (DatabaseCache cache) id =
    case GUID.dictGet (GUID.toCommon id) cache.common of
        Just _ ->
            True

        Nothing ->
            False


getValue :
    (DatabaseCachePayload -> GUIDDict t2 payload)
    -> (payload -> value)
    -> (GUID t1 -> GUID t2)
    -> DatabaseCache
    -> GUID t1
    -> Maybe value
getValue dictAccessor valueAccessor mapId (DatabaseCache cache) id =
    GUID.dictGet (mapId id) (dictAccessor cache)
        |> Maybe.map valueAccessor


name : DatabaseCache -> GUID a -> Maybe String
name =
    getValue .common .name GUID.toCommon


nameAlias : DatabaseCache -> GUID a -> Maybe String
nameAlias cache id =
    getValue .common .alias GUID.toCommon cache id
        |> Maybe.andThen identity


details : DatabaseCache -> GUID a -> Maybe String
details =
    getValue .common .details GUID.toCommon


commonPayload : DatabaseCache -> GUID a -> Maybe CommonPayload
commonPayload =
    getValue .common identity GUID.toCommon


quirkPayload : DatabaseCache -> GUID GUID.QuirkMechanicky -> Maybe QuirkPayload
quirkPayload =
    getValue .specialniSchopnost identity identity


isDefensive : DatabaseCache -> GUID GUID.Heslo -> Bool
isDefensive cache id =
    getValue .heslo .defensivni identity cache id
        |> Maybe.withDefault False


allowZakladniUtok : DatabaseCache -> GUID GUID.Zasah -> Bool
allowZakladniUtok cache id =
    getValue .zasah .zakladniUtok identity cache id
        |> Maybe.withDefault False


treeNameFromJutsu : DatabaseCache -> Maybe (GUID GUID.Jutsu) -> Maybe String
treeNameFromJutsu cache jutsuId =
    jutsuId
        |> Maybe.andThen (getValue .jutsu .strom identity cache)
        |> Maybe.andThen (name cache)



-- VALIDATE


type alias StringValidate guidTyp =
    DatabaseCache -> String -> Maybe (GUID guidTyp)


stringValidate : (DatabaseCachePayload -> GUIDDict t a) -> StringValidate t
stringValidate getDict (DatabaseCache cache) value =
    GUID.parseAgaints (getDict cache) value


stringValidateTree : StringValidate GUID.Tree
stringValidateTree =
    stringValidate .tree


stringValidateSpecialniSchopnost : StringValidate GUID.QuirkMechanicky
stringValidateSpecialniSchopnost =
    stringValidate .specialniSchopnost


type alias SimpleListDecoderUntrusted guidTyp =
    Json.Decode.Decoder (DatabaseCache -> List (GUID guidTyp))


simpleListDecoderJsonUntrustedBase : (DatabaseCachePayload -> GUIDDict t a) -> Json.Decode.Decoder (DatabaseCache -> List (GUID t))
simpleListDecoderJsonUntrustedBase getValidateDict =
    let
        lazyCacheToDict : (GUIDDict t a -> List (GUID t)) -> DatabaseCache -> List (GUID t)
        lazyCacheToDict validate (DatabaseCache cache) =
            validate (getValidateDict cache)
    in
    GUID.simpleListDecoderJsonUntrusted
        |> Json.Decode.map lazyCacheToDict


simpleJutsuListDecoderJsonUntrusted : SimpleListDecoderUntrusted GUID.Jutsu
simpleJutsuListDecoderJsonUntrusted =
    simpleListDecoderJsonUntrustedBase .jutsu


simpleNeschopnostListDecoderJsonUntrusted : SimpleListDecoderUntrusted GUID.Neschopnost
simpleNeschopnostListDecoderJsonUntrusted =
    Json.Decode.map2 (\a b cache -> a cache ++ b cache)
        (simpleListDecoderJsonUntrustedBase (always getValidateDictElementNeschopnost))
        (simpleListDecoderJsonUntrustedBase (.tree >> GUID.dictMapTyp GUID.toNeschopnost))


getValidateDictElementNeschopnost : GUIDDict GUID.Neschopnost ()
getValidateDictElementNeschopnost =
    GUID.dictSet GUID.const.neschopnostElementy () GUID.dictEmpty


simpleListDecoderJsonUntrustedSpecialni : SimpleListDecoderUntrusted GUID.QuirkMechanicky
simpleListDecoderJsonUntrustedSpecialni =
    simpleListDecoderJsonUntrustedBase .specialniSchopnost


type alias SingleDecoderUntrusted t =
    Json.Decode.Decoder (DatabaseCache -> Maybe (GUID t))


singleDecoderJsonUntrusted : (DatabaseCachePayload -> GUIDDict t a) -> Json.Decode.Decoder (DatabaseCache -> Maybe (GUID t))
singleDecoderJsonUntrusted getValidateDict =
    Json.Decode.string
        |> Json.Decode.map (\raw cache -> GUID.parseAgaints (getValidateDict (payload cache)) raw)


elementDecoderJsonUntrusted : SingleDecoderUntrusted GUID.Element
elementDecoderJsonUntrusted =
    singleDecoderJsonUntrusted .element



-- GET ALL


type alias GetAllWithName t =
    DatabaseCache -> List ( GUID t, String )


getAllWithName : (DatabaseCachePayload -> GUIDDict t p) -> GetAllWithName t
getAllWithName getter (DatabaseCache cache) =
    let
        withName : ( GUID t, p ) -> Maybe ( GUID t, String )
        withName ( id, _ ) =
            GUID.dictGet (GUID.toCommon id) cache.common
                |> Maybe.map (\common -> ( id, common.name ))
    in
    getter cache
        |> GUID.dictToItems
        |> List.filterMap withName


elements : GetAllWithName GUID.Element
elements =
    getAllWithName .element



-- ATTRIBUTES


getAttributeDefaults : DatabaseCache -> List ( GUID GUID.Atribut, Int )
getAttributeDefaults (DatabaseCache cache) =
    GUID.dictToItems cache.atribut
        |> List.map (Tuple.mapSecond .defaultValue)


sumAttributesWithDefaults : DatabaseCache -> List ( GUID GUID.Atribut, Int ) -> List ( GUID GUID.Atribut, Int )
sumAttributesWithDefaults cache values =
    GUID.dictEmpty
        |> sumAttributesWithDefaultsIter (getAttributeDefaults cache)
        |> sumAttributesWithDefaultsIter values
        |> GUID.dictToItems


sumAttributesWithDefaultsIter : List ( GUID GUID.Atribut, Int ) -> GUIDDict GUID.Atribut Int -> GUIDDict GUID.Atribut Int
sumAttributesWithDefaultsIter items dict =
    case items of
        ( id, value ) :: rest ->
            dict
                |> GUID.dictGet id
                |> Maybe.withDefault 0
                |> (+) value
                |> (\v -> GUID.dictSet id v dict)
                |> sumAttributesWithDefaultsIter rest

        [] ->
            dict



-- REGISTER


registerIterHelper : (a -> DatabaseCache -> DatabaseCache) -> List a -> DatabaseCache -> DatabaseCache
registerIterHelper registerFn items cache =
    List.foldl registerFn cache items


type alias RegisterCommon =
    CommonPayload -> DatabaseCache -> DatabaseCache


type alias RegisterSpecific typ payload =
    GUID typ -> payload -> RegisterCommon


registerCommon : GUID t -> RegisterCommon
registerCommon id common (DatabaseCache cache) =
    let
        commonId : GUID GUID.Common
        commonId =
            GUID.toCommon id
    in
    DatabaseCache
        { cache
            | common = GUID.dictSet commonId common cache.common
            , idConflicts = GUID.setAddIf commonId (GUID.dictGet commonId cache.common /= Nothing) cache.idConflicts
        }


registerCommonCompat : RegisterSpecific t ()
registerCommonCompat id _ =
    registerCommon id


registerSpecific : (DatabaseCachePayload -> GUIDDict typ payload) -> (GUIDDict typ payload -> DatabaseCachePayload -> DatabaseCachePayload) -> RegisterSpecific typ payload
registerSpecific get set id specific common (DatabaseCache cache) =
    DatabaseCache (set (GUID.dictSet id specific (get cache)) cache)
        |> registerCommon id common


registerHeslo : RegisterSpecific GUID.Heslo HesloPayload
registerHeslo =
    registerSpecific .heslo (\dict cache -> { cache | heslo = dict })


registerJutsu : RegisterSpecific GUID.Jutsu JutsuPayload
registerJutsu =
    registerSpecific .jutsu (\dict cache -> { cache | jutsu = dict })


registerAtribut : RegisterSpecific GUID.Atribut AtributPayload
registerAtribut =
    registerSpecific .atribut (\dict cache -> { cache | atribut = dict })


registerZasah : RegisterSpecific GUID.Zasah ZasahPayload
registerZasah =
    registerSpecific .zasah (\dict cache -> { cache | zasah = dict })


registerElement : RegisterSpecific GUID.Element ElementPayload
registerElement =
    registerSpecific .element (\dict cache -> { cache | element = dict })


registerTree : RegisterSpecific GUID.Tree TreePayload
registerTree =
    registerSpecific .tree (\dict cache -> { cache | tree = dict })


registerQuirk : RegisterSpecific GUID.QuirkMechanicky QuirkPayload
registerQuirk =
    registerSpecific .specialniSchopnost (\dict cache -> { cache | specialniSchopnost = dict })



-- DECODE


type alias MapCache =
    DatabaseCache -> DatabaseCache


commonDecoder : String -> Xml.Decode.Decoder MapCache
commonDecoder =
    definiceDecoder (Xml.Decode.succeed ()) registerCommonCompat


hesloDecoder : HesloPayload -> String -> Xml.Decode.Decoder MapCache
hesloDecoder hesloPayload =
    definiceDecoder (Xml.Decode.succeed hesloPayload) registerHeslo


atributDecoder : String -> Xml.Decode.Decoder MapCache
atributDecoder =
    definiceDecoder atributPayloadDecoder registerAtribut


atributPayloadDecoder : Xml.Decode.Decoder AtributPayload
atributPayloadDecoder =
    Xml.Decode.map AtributPayload
        (Xml.Decode.intAttr "default")


zasahDecoder : String -> Xml.Decode.Decoder MapCache
zasahDecoder =
    definiceDecoder zasahPayloadDecoder registerZasah


zasahPayloadDecoder : Xml.Decode.Decoder ZasahPayload
zasahPayloadDecoder =
    Xml.Decode.map ZasahPayload
        (Xml.Decode.boolAttr "zakladní-útok")


elementDecoder : String -> Xml.Decode.Decoder MapCache
elementDecoder =
    definiceDecoder (Xml.Decode.succeed {}) registerElement


definiceDecoder :
    Xml.Decode.Decoder a
    -> (GUID t -> a -> CommonPayload -> DatabaseCache -> DatabaseCache)
    -> String
    -> Xml.Decode.Decoder MapCache
definiceDecoder specificDecoder register_ tag =
    Xml.Decode.with
        (Xml.Decode.path [ tag ] (Xml.Decode.list (definiceItemDecoder specificDecoder)))
        (Xml.Decode.succeed
            << List.foldl
                (\( id, ( common, specific ) ) acc -> acc >> register_ id specific common)
                identity
        )


definiceItemDecoder : Xml.Decode.Decoder a -> Xml.Decode.Decoder ( GUID t, ( CommonPayload, a ) )
definiceItemDecoder specificDecoder =
    Xml.Decode.map2 Tuple.pair
        GUID.idDecoder
        (Xml.Decode.map2 Tuple.pair
            commonPayloadDecoder
            specificDecoder
        )


commonPayloadDecoder : Xml.Decode.Decoder CommonPayload
commonPayloadDecoder =
    Xml.Decode.map4 CommonPayload
        (Xml.Decode.path [ "jméno" ] (Xml.Decode.single Xml.Decode.string))
        (Xml.Decode.path [ "popisek" ] (Xml.Decode.single Xml.Decode.string))
        (Xml.Decode.path [ "alias" ] (Xml.Decode.single Xml.Decode.string)
            |> Xml.Decode.maybe
        )
        (Xml.Decode.path [ "zobrazení" ] (Xml.Decode.single Xml.Decode.node)
            |> Xml.Decode.maybe
            |> Xml.Decode.andThen zobrazeniDecoderInner
        )


zobrazeniDecoderInner : Maybe XmlParser.Node -> Xml.Decode.Decoder (Maybe (ZobrazeniParams -> Html Never))
zobrazeniDecoderInner node =
    case node of
        Just (XmlParser.Element "zobrazení" [] children) ->
            Xml.Decode.succeed (Just (\params -> Html.span [] (List.map (zobrazeniDecoderInnerRender params) children)))

        Nothing ->
            Xml.Decode.succeed Nothing

        _ ->
            Xml.Decode.fail "invalid zobrazeni"


zobrazeniDecoderInnerRender : ZobrazeniParams -> XmlParser.Node -> Html Never
zobrazeniDecoderInnerRender params part =
    case part of
        XmlParser.Element "jméno" [] [] ->
            params.jmeno

        XmlParser.Element "počet" [] [] ->
            Html.text (String.fromInt params.pocet)

        XmlParser.Text text ->
            Html.text text

        _ ->
            -- TODO: propagate Xml.Decode error
            Html.text "ERROR"



-- VIEW


viewDefinition : DatabaseCache -> GUID t -> Html msg
viewDefinition cache id =
    Html.div
        []
        [ viewSingleValueAs .name UI.Definition.viewName cache id
        , Html.text ": "
        , viewSingleValueAs .details Html.text cache id
        ]


viewDefinitionMinor : DatabaseCache -> GUID t -> Html msg
viewDefinitionMinor cache id =
    Html.div
        []
        [ viewSingleValueAs .name UI.minor cache id
        , UI.minor ": "
        , viewSingleValueAs .details Html.text cache id
        ]


viewName : DatabaseCache -> GUID t -> Html msg
viewName =
    viewSingleValueAs .name UI.Definition.viewName


viewSingleValueAs : (CommonPayload -> value) -> (value -> Html msg) -> DatabaseCache -> GUID t -> Html msg
viewSingleValueAs accessor render cache id =
    case getValue .common accessor GUID.toCommon cache id of
        Just value ->
            render value

        Nothing ->
            GUID.dump id


viewSingleValueAsBlock : DatabaseCache -> GUID t -> Html msg
viewSingleValueAsBlock cache id =
    case commonPayload cache id of
        Just data ->
            Html.div
                []
                [ UI.Definition.viewName data.name
                , case data.alias of
                    Just alias_ ->
                        UI.minor (" (" ++ alias_ ++ ")")

                    Nothing ->
                        Html.text ""
                , UI.space (Css.rem 0.1)
                , Html.text data.details
                ]

        Nothing ->
            Html.div
                []
                [ GUID.dump id
                ]



-- VIEW VALUES


viewValues : String -> DatabaseCache -> List ( GUID t, Int ) -> List (Html msg)
viewValues prefix cache list =
    viewValuesVsechny prefix cache (List.map (Tuple.mapSecond Prave) list)


viewValuesVsechny : String -> DatabaseCache -> List ( GUID t, Vsechny Int ) -> List (Html msg)
viewValuesVsechny prefix cache list =
    let
        viewItem : ( GUID t, Vsechny Int ) -> Html msg
        viewItem ( id, val ) =
            Html.div
                []
                [ viewSingleValueAs .name UI.Definition.viewLabel cache id
                , UI.Definition.viewName (prefix ++ vsechnyToString val)
                ]
    in
    list
        -- TODO: order
        |> List.filter (Tuple.second >> isNonZero)
        |> List.map viewItem


viewValuesAlias : String -> DatabaseCache -> List ( GUID t, Int ) -> List (Html msg)
viewValuesAlias prefix cache list =
    viewValuesAliasVsechny prefix cache (List.map (Tuple.mapSecond Prave) list)


viewValuesAliasVsechny : String -> DatabaseCache -> List ( GUID t, Vsechny Int ) -> List (Html msg)
viewValuesAliasVsechny prefix cache list =
    let
        viewItem : ( GUID t, Vsechny Int ) -> Html msg
        viewItem ( id, val ) =
            Html.div
                []
                [ viewSingleValueAs (\v -> v.alias |> Maybe.withDefault v.name) UI.Definition.viewLabel cache id
                , UI.Definition.viewName (prefix ++ vsechnyToString val)
                ]
    in
    list
        -- TODO: order
        |> List.filter (Tuple.second >> isNonZero)
        |> List.map viewItem


vsechnyToString : Vsechny Int -> String
vsechnyToString val =
    case val of
        Vsechny ->
            "Všechny"

        Prave valInt ->
            String.fromInt valInt


isNonZero : Vsechny Int -> Bool
isNonZero x =
    case x of
        Vsechny ->
            True

        Prave intVal ->
            intVal /= 0



-- VIEW LIST BUILDER


type alias ViewListBuilderPayload t =
    { sep : String
    , label : Maybe String
    , list : List (GUID t)
    , suffix : List (Html Never)
    , comment : Maybe String
    , short : Bool
    , renderAlias : Bool
    }


type ViewListBuilder t
    = ViewListBuilder (ViewListBuilderPayload t)


viewListBuilder : String -> List (GUID t) -> ViewListBuilder t
viewListBuilder sep list =
    ViewListBuilder
        { sep = sep
        , label = Nothing
        , list = list
        , suffix = []
        , comment = Nothing
        , short = False
        , renderAlias = False
        }


viewSingleValueBuilder : Maybe (GUID t) -> ViewListBuilder t
viewSingleValueBuilder value =
    viewListBuilder "" (GUID.listFromMaybe value)


withSuffix : List String -> ViewListBuilder t -> ViewListBuilder t
withSuffix suffix (ViewListBuilder builder) =
    ViewListBuilder
        { builder | suffix = List.map UI.Definition.viewName suffix }


withSuffixManual : List (Html Never) -> ViewListBuilder t -> ViewListBuilder t
withSuffixManual suffix (ViewListBuilder builder) =
    ViewListBuilder
        { builder | suffix = suffix }


withLabel : String -> ViewListBuilder t -> ViewListBuilder t
withLabel label (ViewListBuilder builder) =
    ViewListBuilder
        { builder | label = Just label }


withComment : Maybe String -> ViewListBuilder t -> ViewListBuilder t
withComment comment (ViewListBuilder builder) =
    ViewListBuilder
        { builder | comment = comment }


withShort : Bool -> ViewListBuilder t -> ViewListBuilder t
withShort short (ViewListBuilder builder) =
    ViewListBuilder
        { builder | short = short }


withAlias : Bool -> ViewListBuilder t -> ViewListBuilder t
withAlias renderAlias (ViewListBuilder builder) =
    ViewListBuilder
        { builder | renderAlias = renderAlias }


buildBlock : DatabaseCache -> ViewListBuilder t -> Html msg
buildBlock cache (ViewListBuilder builder) =
    if List.isEmpty (renderListContent cache builder) then
        Html.text ""

    else
        UI.Definition.viewSimpleBlockInner
            builder.short
            (Maybe.withDefault "" builder.label)
            [ ViewListBuilder { builder | label = Nothing, comment = Nothing }
                |> buildInline cache
            , renderComment builder
            ]


buildInline : DatabaseCache -> ViewListBuilder t -> Html msg
buildInline cache (ViewListBuilder builder) =
    UI.Definition.listLabeledWithComment
        builder.sep
        (Maybe.withDefault "" builder.label)
        (renderListContent cache builder)
        builder.comment


buildString : DatabaseCache -> ViewListBuilder t -> String
buildString cache (ViewListBuilder builder) =
    [ case builder.label of
        Just label ->
            label ++ ":"

        Nothing ->
            ""
    , case List.Extra.unconsLast (List.filterMap (name cache) builder.list) of
        --TODO: alias
        Just ( last, first :: body ) ->
            String.concat
                [ String.join ", " (first :: body)
                , addSpacesToSeparator builder.sep
                , last
                ]

        Just ( last, [] ) ->
            last

        Nothing ->
            ""
    , getComment builder
    ]
        |> List.filter (not << String.isEmpty)
        |> String.join " "


renderListContent : DatabaseCache -> ViewListBuilderPayload t -> List (Html msg)
renderListContent cache builder =
    let
        renderItem : ( GUID t, Int ) -> Html msg
        renderItem ( id, count ) =
            Html.span
                []
                [ renderNameCounted cache ( id, count )
                , renderNameCountedAlias cache builder id
                ]
    in
    List.map renderItem (GUID.countEquals builder.list)
        ++ List.map (Html.map never) builder.suffix


renderNameCounted : DatabaseCache -> ( GUID t, Int ) -> Html msg
renderNameCounted cache ( id, count ) =
    case getValue .common .zobrazeni GUID.toCommon cache id of
        Just (Just zobrazeni) ->
            zobrazeni { pocet = count, jmeno = viewName cache id }
                |> Html.map never

        _ ->
            UI.Definition.multiply ( viewName cache id, count )


renderNameCountedAlias : DatabaseCache -> ViewListBuilderPayload t -> GUID t -> Html msg
renderNameCountedAlias cache builder id =
    if builder.short || not builder.renderAlias then
        Html.text ""

    else
        case nameAlias cache id of
            Just al ->
                UI.gray (" (" ++ al ++ ")")

            Nothing ->
                Html.text ""


getComment : ViewListBuilderPayload t -> String
getComment builder =
    if builder.short then
        ""

    else
        case builder.comment of
            Just text ->
                "(" ++ text ++ ")"

            Nothing ->
                ""


renderComment : ViewListBuilderPayload t -> Html msg
renderComment builder =
    case getComment builder of
        "" ->
            Html.text ""

        comment ->
            UI.minor comment


addSpacesToSeparator : String -> String
addSpacesToSeparator sep =
    case String.trim sep of
        "," ->
            ", "

        _ ->
            " " ++ sep ++ " "
