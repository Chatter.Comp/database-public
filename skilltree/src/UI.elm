module UI exposing
    ( StylableElement
    , borderNormal
    , borderNormalWidth
    , col
    , cssBgCol
    , displayContents
    , error
    , gray
    , highlight
    , little
    , minor
    , normal
    , row
    , space
    , textGray
    , textHighlight
    , textLittle
    , textMinor
    , varColBg
    , varColFg
    , varColMinorBg
    , varColMinorFg
    , whenAttr
    , whenCss
    , whenHtml
    , wrapWithStyle
    , wrapWithStyleWhen
    )

import Css
import Html.Styled as Html exposing (Html)
import Html.Styled.Attributes as Attrs


type alias StylableElement msg =
    List Css.Style -> Html msg


ifElse : Bool -> a -> a -> a
ifElse predicate main fallback =
    if predicate then
        main

    else
        fallback


whenCss : Bool -> Css.Style -> Css.Style
whenCss predicate style =
    ifElse predicate style (Css.batch [])


whenHtml : Bool -> Html msg -> Html msg
whenHtml predicate element =
    ifElse predicate element (Html.text "")


whenAttr : Bool -> Html.Attribute msg -> Html.Attribute msg
whenAttr predicate attr =
    ifElse predicate attr (Attrs.class "")


wrapWithStyle : Css.Style -> Html msg -> Html msg
wrapWithStyle style element =
    Html.span
        [ Attrs.css [ style ] ]
        [ element ]


wrapWithStyleWhen : Css.Style -> Bool -> Html msg -> Html msg
wrapWithStyleWhen style predicate element =
    Html.span
        [ Attrs.css [ whenCss predicate style ] ]
        [ element ]



-- elements


minor : String -> Html msg
minor txt =
    Html.span
        [ Attrs.css [ textMinor ] ]
        [ Html.text txt ]


gray : String -> Html msg
gray txt =
    Html.span
        [ Attrs.css [ textGray ] ]
        [ Html.text txt ]


little : String -> Html msg
little txt =
    Html.span
        [ Attrs.css [ textLittle ] ]
        [ Html.text txt ]


highlight : String -> Html msg
highlight txt =
    Html.span
        [ Attrs.css [ textHighlight ] ]
        [ Html.text txt ]



-- common styles


type alias VarCol =
    { bg : String
    , minorBg : String
    , fg : String
    , minorFg : String
    }


varColNames : VarCol
varColNames =
    { bg = "--col--bg"
    , minorBg = "--col--minor-bg"
    , fg = "--col--fg"
    , minorFg = "--col--minor-fg"
    }


varColNormal : VarCol
varColNormal =
    { bg = "var(--col-normal--bg, #fff)"
    , minorBg = "var(--col-normal--minor-bg, #ddd)"
    , fg = "var(--col-normal--fg, #000)"
    , minorFg = "var(--col-normal--minor-fg, #767676)"
    }


varColError : VarCol
varColError =
    { bg = "var(--col-error--bg, #faa)"
    , minorBg = "var(--col-normal--minor-bg, #fcc)"
    , fg = "var(--col-error--fg, #000)"
    , minorFg = "var(--col-error--minor-fg, #767676)"
    }


varColHighlight : VarCol
varColHighlight =
    { bg = "var(--col-highlight--bg, #DDFFDD)"
    , minorBg = "var(--col-highlight--minor-bg, #DDFFDD)"
    , fg = "var(--col-highlight--fg, #000)"
    , minorFg = "var(--col-highlight--minor-fg, #595959)"
    }


var : String -> String -> Css.Style
var prop varName =
    Css.property prop ("var(" ++ varName ++ ")")


hex : String -> String
hex val =
    if String.startsWith "#" val || String.startsWith "var" val then
        val

    else
        "#" ++ val


assignVars : VarCol -> Css.Style
assignVars vars =
    Css.batch
        [ Css.property varColNames.bg (hex vars.bg)
        , Css.property varColNames.minorBg (hex vars.minorBg)
        , Css.property varColNames.fg (hex vars.fg)
        , Css.property varColNames.minorFg (hex vars.minorFg)
        , var "color" varColNames.fg
        , var "background-color" varColNames.bg
        ]


varColBg : String
varColBg =
    "var(" ++ varColNames.bg ++ ")"


varColMinorBg : String
varColMinorBg =
    "var(" ++ varColNames.minorBg ++ ")"


varColFg : String
varColFg =
    "var(" ++ varColNames.fg ++ ")"


varColMinorFg : String
varColMinorFg =
    "var(" ++ varColNames.minorFg ++ ")"


normal : Css.Style
normal =
    assignVars varColNormal


error : Css.Style
error =
    assignVars varColError


textHighlight : Css.Style
textHighlight =
    Css.batch
        [ assignVars varColHighlight
        , Css.property "box-shadow" ("0 0 0.1rem 0.2rem " ++ varColBg)
        ]


textGray : Css.Style
textGray =
    var "color" varColNames.minorFg


textMinor : Css.Style
textMinor =
    Css.batch
        [ textGray
        , textLittle
        ]


textLittle : Css.Style
textLittle =
    Css.fontSize (Css.rem 0.8)


borderNormalWidth : String
borderNormalWidth =
    "1px"


borderNormal : Css.Style
borderNormal =
    Css.property "border" (borderNormalWidth ++ " solid " ++ varColFg)



-- layout


row : List (Html.Attribute msg) -> List (Html msg) -> Html msg
row =
    Html.styled Html.div
        [ Css.displayFlex
        , Css.property "gap" "0.5rem"
        ]


col : List (Html.Attribute msg) -> List (Html msg) -> Html msg
col =
    Html.styled Html.div
        [ Css.displayFlex
        , Css.flexDirection Css.column
        , Css.property "gap" "1rem"
        ]


space : Css.LengthOrAuto compatible -> Html msg
space height =
    Html.div
        [ Attrs.css
            [ Css.height height
            ]
        ]
        []


displayContents : List (Html msg) -> Html msg
displayContents =
    Html.div
        [ Attrs.css
            [ Css.property "display" "contents"
            ]
        ]



-- SPECIFIC


cssBgCol : Maybe String -> Css.Style
cssBgCol color =
    case color of
        Just bgColorOverride ->
            Css.batch
                [ Css.property "--col-normal--bg" (hex bgColorOverride)
                , Css.property "--col-normal--minor-bg" (hex bgColorOverride)
                , assignVars
                    { varColNormal | bg = bgColorOverride, minorBg = bgColorOverride }
                ]

        Nothing ->
            Css.batch []
