module Jutsu exposing
    ( CheckboxOptions
    , Jutsu
    , JutsuById
    , PermalockReason(..)
    , access
    , decoder
    , extractTerms
    , givenAttributes
    , groupByLevel
    , id
    , level
    , parentJutsu
    , tree
    , updateCache
    , viewCheckbox
    , viewDetail
    , viewTableContainer
    , viewTableHeader
    , viewTableRow
    )

import Css
import Css.Global
import DatabaseCache exposing (DatabaseCache)
import GUID exposing (GUID, GUIDDict, GUIDSet)
import Html.Styled as Html exposing (Html, a)
import Html.Styled.Attributes as Attrs
import Html.Styled.Lazy
import JutsuAccess exposing (JutsuAccess)
import JutsuEffect exposing (Effect)
import JutsuEffectVylepseni
import List.Extra
import UI
import UI.BlockQuote
import UI.Checkbox
import UI.Definition
import UI.Tooltip
import Utils exposing (ListNotEmpty, Vsechny(..))
import Xml.Decode


type Jutsu
    = Jutsu SharedPayload JutsuSpecific


type JutsuSpecific
    = Aktivni AktivniPayload
    | Pasivni
    | Vylepseni (GUID GUID.Jutsu)


type alias SharedPayload =
    { id : GUID GUID.Jutsu
    , name : String
    , alias : List String
    , level : Int
    , tree : GUID GUID.Tree
    , flavour : String
    , access : JutsuAccess
    , prerekvizity : GUIDSet GUID.Jutsu
    , pasivni : JutsuEffect.Pasivni
    }


type alias AktivniPayload =
    { typ : GUID GUID.JutsuType
    , modifikator : JutsuEffectVylepseni.Group (GUIDSet GUID.Common)
    , cena : JutsuEffectVylepseni.Group (GUIDDict GUID.Common (Vsechny Int))
    , cooldown : Maybe (GUID GUID.Common)
    , effects : JutsuEffectVylepseni.Group (List Effect)
    }


resolveVylepseniCena : JutsuEffectVylepseni.Reducer (GUIDDict GUID.Common (Vsechny Int))
resolveVylepseniCena =
    JutsuEffectVylepseni.resolveGUIDDict


resolveVylepseniModifikator : JutsuEffectVylepseni.Reducer (GUIDSet GUID.Common)
resolveVylepseniModifikator =
    JutsuEffectVylepseni.resolveGUIDSet



-- GETTERS


id : Jutsu -> GUID GUID.Jutsu
id (Jutsu jutsu _) =
    jutsu.id


name : Jutsu -> String
name (Jutsu jutsu _) =
    jutsu.name


level : Jutsu -> Int
level (Jutsu jutsu _) =
    jutsu.level


tree : Jutsu -> GUID GUID.Tree
tree (Jutsu payload _) =
    payload.tree


access : Jutsu -> JutsuAccess
access (Jutsu jutsu _) =
    jutsu.access


givenAttributes : Jutsu -> List JutsuEffect.Atribut
givenAttributes (Jutsu jutsu _) =
    JutsuEffect.givenAttributes jutsu.pasivni


parentJutsu : Jutsu -> Maybe (GUID GUID.Jutsu)
parentJutsu (Jutsu _ spec) =
    case spec of
        Vylepseni parentId ->
            Just parentId

        _ ->
            Nothing



-- CENA


instant : ResolutionOrder -> AktivniPayload -> Bool
instant resolutionOrder payload =
    payload.cena
        |> resolveVylepseniCena resolutionOrder
        |> .merged
        |> instantCena


instantCena : GUIDDict GUID.Common (Vsechny Int) -> Bool
instantCena dict =
    dict
        |> GUID.dictGet GUID.const.cenaPecet
        |> Maybe.withDefault (Prave 0)
        |> (==) (Prave 0)


freeCena : GUIDDict GUID.Common (Vsechny Int) -> Bool
freeCena dict =
    dict
        |> GUID.dictRemove GUID.const.cenaPecet
        |> GUID.dictToItems
        |> List.map Tuple.second
        |> List.all ((==) (Prave 0))



-- EXTRACT TERMS


extractTerms : JutsuById -> Jutsu -> GUIDSet GUID.Common
extractTerms jutsuById (Jutsu jutsu jutsuSpecific) =
    let
        ( Jutsu _ baseSpecific, resolutionOrder ) =
            vylepseniResolutionOrder jutsuById (Jutsu jutsu jutsuSpecific)
    in
    GUID.setEmpty
        |> GUID.setUnion (JutsuEffect.extractPasivniTerms jutsu.pasivni)
        |> GUID.setUnion (extractTermsSpecific resolutionOrder baseSpecific)


extractTermsSpecific : ResolutionOrder -> JutsuSpecific -> GUIDSet GUID.Common
extractTermsSpecific resolutionOrder spec =
    case spec of
        Aktivni payload ->
            GUID.setEmpty
                |> GUID.setUnion
                    (payload.cena
                        |> resolveVylepseniCena resolutionOrder
                        |> .merged
                        |> GUID.dictToItems
                        -- TODO: filter >0?
                        |> List.map Tuple.first
                        |> GUID.setFromList
                    )
                |> GUID.setUnion (GUID.setFromMaybe payload.cooldown)
                |> GUID.setAdd (GUID.toCommon payload.typ)
                |> GUID.setUnion (extractTermsEffects resolutionOrder payload)

        Pasivni ->
            GUID.setEmpty

        Vylepseni _ ->
            GUID.setEmpty


extractTermsEffects : ResolutionOrder -> AktivniPayload -> GUIDSet GUID.Common
extractTermsEffects resolutionOrder jutsu =
    jutsu.effects
        |> JutsuEffectVylepseni.resolveReplace [] resolutionOrder
        |> .merged
        |> List.map JutsuEffect.extractTerms
        |> List.foldl GUID.setUnion GUID.setEmpty



-- JUTSU BY ID


type alias JutsuById =
    GUIDDict GUID.Jutsu Jutsu


{-| Prvni je zakladni technika, potom jeji prvni vylepseni, ..., posledni je aktualni technika
-}
type alias ResolutionOrder =
    ListNotEmpty (GUID GUID.Jutsu)


vylepseniResolutionOrder : JutsuById -> Jutsu -> ( Jutsu, ResolutionOrder )
vylepseniResolutionOrder jutsuById jutsu =
    vylepseniResolutionOrderInner jutsuById jutsu []


vylepseniResolutionOrderInner : JutsuById -> Jutsu -> List (GUID GUID.Jutsu) -> ( Jutsu, ResolutionOrder )
vylepseniResolutionOrderInner jutsuById jutsu acc =
    parentJutsu jutsu
        |> Maybe.andThen (\parentId -> GUID.dictGet parentId jutsuById)
        |> Maybe.map (\parentJutsu_ -> vylepseniResolutionOrderInner jutsuById parentJutsu_ (id jutsu :: acc))
        |> Maybe.withDefault
            ( jutsu
            , (id jutsu :: acc)
                |> Utils.listNotEmpty
                |> Maybe.withDefault ( id jutsu, [] )
            )


{-| Remove the last element
-}
resolutionOrderParentsOnly : ResolutionOrder -> List (GUID GUID.Jutsu)
resolutionOrderParentsOnly resolutionOrder =
    resolutionOrder
        |> Utils.cons
        |> List.Extra.unconsLast
        |> Maybe.map Tuple.second
        |> Maybe.withDefault []



-- XML


decoderPostProcess : SharedPayload -> JutsuSpecific -> Jutsu
decoderPostProcess shared specific =
    Jutsu
        { shared
            | prerekvizity =
                shared.prerekvizity
                    |> GUID.setAddMaybe (parentJutsu (Jutsu shared specific))
        }
        specific


decoder : GUID GUID.Tree -> Xml.Decode.Decoder Jutsu
decoder treeId =
    GUID.idDecoder
        |> Xml.Decode.andThen
            (\jutsuId ->
                Xml.Decode.map2 decoderPostProcess
                    (sharedPayloadDecoder treeId)
                    (jutsuPayloadDecoder jutsuId)
            )


sharedPayloadDecoder : GUID GUID.Tree -> Xml.Decode.Decoder SharedPayload
sharedPayloadDecoder treeId =
    Xml.Decode.succeed SharedPayload
        |> Xml.Decode.andMap GUID.idDecoder
        |> Xml.Decode.requiredPath [ "jméno" ] (Xml.Decode.single Xml.Decode.string)
        |> Xml.Decode.requiredPath [ "alias" ] (Xml.Decode.list Xml.Decode.string)
        |> Xml.Decode.requiredPath [ "úroveň" ] (Xml.Decode.single Xml.Decode.int)
        |> Xml.Decode.andMap (Xml.Decode.succeed treeId)
        |> Xml.Decode.optionalPath [ "flavour" ] (Xml.Decode.single Xml.Decode.string) ""
        |> Xml.Decode.andMap JutsuAccess.accessDecoder
        |> Xml.Decode.andMap (Xml.Decode.map GUID.setFromList (GUID.refListPathDecoder [ "prerekvizity", "technika" ]))
        |> Xml.Decode.andMap JutsuEffect.pasivniDecoder


jutsuPayloadDecoder : GUID GUID.Jutsu -> Xml.Decode.Decoder JutsuSpecific
jutsuPayloadDecoder jutsuId =
    Xml.Decode.oneOf
        [ jutsuPayloadDecoderAktivni jutsuId
        , jutsuPayloadDecoderVylepseni
        ]


jutsuPayloadDecoderAktivni : GUID GUID.Jutsu -> Xml.Decode.Decoder JutsuSpecific
jutsuPayloadDecoderAktivni jutsuId =
    Xml.Decode.succeed AktivniPayload
        |> Xml.Decode.requiredPath [ "typ-efektu" ] (Xml.Decode.single GUID.refDecoder)
        |> Xml.Decode.andMap (modifikatorDecoder jutsuId)
        |> Xml.Decode.andMap (Xml.Decode.map (JutsuEffectVylepseni.map GUID.dictFromItems) (cenaDecoder jutsuId))
        |> Xml.Decode.optionalPath [ "cooldown" ] (Xml.Decode.single (Xml.Decode.map Just GUID.refDecoder)) Nothing
        |> Xml.Decode.andMap (JutsuEffect.effectsDecoder jutsuId)
        |> Xml.Decode.map Aktivni


jutsuPayloadDecoderVylepseni : Xml.Decode.Decoder JutsuSpecific
jutsuPayloadDecoderVylepseni =
    Xml.Decode.succeed Vylepseni
        |> Xml.Decode.requiredPath [ "vylepšuje-techniku" ] (Xml.Decode.single GUID.refDecoder)


modifikatorDecoder : GUID GUID.Jutsu -> Xml.Decode.Decoder (JutsuEffectVylepseni.Group (GUIDSet GUID.Common))
modifikatorDecoder jutsuId =
    JutsuEffectVylepseni.decoder [ "modifikátor-efektu" ] GUID.refDecoder jutsuId
        |> Xml.Decode.map (JutsuEffectVylepseni.map GUID.setFromList)


cenaDecoder : GUID GUID.Jutsu -> Xml.Decode.Decoder (JutsuEffectVylepseni.Group (List ( GUID GUID.Common, Vsechny Int )))
cenaDecoder jutsuId =
    Xml.Decode.map2 (JutsuEffectVylepseni.merge (++))
        (JutsuEffectVylepseni.decoder [ "cena" ] cenaSingleDecoderPrave jutsuId)
        (JutsuEffectVylepseni.decoder [ "všechny-cena" ] cenaSingleDecoderVsechny jutsuId)


cenaSingleDecoderPrave : Xml.Decode.Decoder ( GUID GUID.Common, Vsechny Int )
cenaSingleDecoderPrave =
    GUID.refIntDecoder
        |> Xml.Decode.map (\( id_, int ) -> ( id_, Prave int ))


cenaSingleDecoderVsechny : Xml.Decode.Decoder ( GUID GUID.Common, Vsechny Int )
cenaSingleDecoderVsechny =
    GUID.refDecoder
        |> Xml.Decode.map (\z -> ( z, Vsechny ))



-- TRANSFORM


groupByLevel : List Jutsu -> List (List Jutsu)
groupByLevel techniky =
    List.Extra.gatherEqualsBy level techniky
        |> List.sortBy (Tuple.first >> level)
        |> List.map Utils.cons



-- CACHE


updateCache : Jutsu -> DatabaseCache -> DatabaseCache
updateCache (Jutsu jutsu _) =
    DatabaseCache.registerJutsu jutsu.id
        { level = jutsu.level
        , strom = jutsu.tree
        }
        { name = jutsu.name
        , details = ""
        , alias = Nothing
        , zobrazeni = Nothing
        }



-- VIEW CHECKBOX


type PermalockReason
    = None
    | CharacterLevel Int
    | Neschopnost
    | Element


permalockReasonMessage : PermalockReason -> String
permalockReasonMessage reason =
    case reason of
        None ->
            ""

        CharacterLevel 1 ->
            "🔒 Tato úroveň bude postavě přístupná až na příštím ročníku"

        CharacterLevel diff ->
            "🔒 Tato úroveň bude postavě přístupná až za " ++ String.fromInt diff ++ " ročníky"

        Neschopnost ->
            "🔒 Postava je v tomto stromě neschopná"

        Element ->
            "🔒 Postava neovládá tento element"


type alias CheckboxOptions msg =
    { selected : Bool
    , locked : Bool
    , permaLock : PermalockReason
    , relevantQuirks : GUIDSet GUID.QuirkMechanicky
    , onChange : Bool -> msg
    , isInUpperHalf : Bool
    }


viewCheckbox : DatabaseCache -> JutsuById -> CheckboxOptions msg -> Jutsu -> Html msg
viewCheckbox cache jutsuById options jutsu =
    UI.Tooltip.view
        { position = tooltipPositionFromLevel (level jutsu)
        , width = Css.rem 30
        , above = not options.isInUpperHalf
        }
        (UI.Checkbox.viewStylable
            { label = name jutsu ++ JutsuAccess.accessSuffix (access jutsu)
            , value = options.selected
            , editability = checkboxEditability options
            , onChange = options.onChange
            }
        )
        -- convert permaLock to string here, for lazy to work
        -- relevantQuirks are directly from model, so identity is stable
        (Html.Styled.Lazy.lazy6 viewDetail
            cache
            jutsuById
            (permalockReasonMessage options.permaLock)
            True
            options.relevantQuirks
            jutsu
        )


checkboxEditability : CheckboxOptions msg -> UI.Checkbox.Editability
checkboxEditability options =
    if options.permaLock /= None then
        UI.Checkbox.PermaLocked

    else if options.locked then
        UI.Checkbox.Locked

    else
        UI.Checkbox.Editable


tooltipPositionFromLevel : Int -> UI.Tooltip.Position
tooltipPositionFromLevel lvl =
    if lvl < 3 then
        UI.Tooltip.Right

    else if lvl < 5 then
        UI.Tooltip.Center

    else
        UI.Tooltip.Left



-- VIEW DETAIL


wrapInParenthesis : String -> String
wrapInParenthesis text =
    if String.length text > 0 then
        "(" ++ text ++ ")"

    else
        ""


wrapInParenthesisInline : List (Html msg) -> Html msg
wrapInParenthesisInline children =
    if List.isEmpty children then
        Html.text ""

    else
        Html.span [] <|
            List.concat
                [ [ Html.text "(" ]
                , children
                , [ Html.text ")" ]
                ]


detailPadding : Css.Style
detailPadding =
    Css.padding2 (Css.rem 0.2) (Css.rem 0.4)


viewDetail : DatabaseCache -> JutsuById -> String -> Bool -> GUIDSet GUID.QuirkMechanicky -> Jutsu -> Html msg
viewDetail cache jutsuById permalockReason helpVisible relevantQuirks (Jutsu jutsu jutsuSpecific) =
    let
        resolutionOrder : ResolutionOrder
        resolutionOrder =
            vylepseniResolutionOrder jutsuById (Jutsu jutsu jutsuSpecific)
                |> Tuple.second
    in
    Html.div
        [ Attrs.css
            [ Css.displayFlex
            , Css.flexDirection Css.column
            , Css.property "gap" "0.4rem"
            , Css.position Css.relative
            , Css.property "max-width" "var(--jutsu-max-width, 30rem)"
            , Css.property "break-inside" "avoid"
            , UI.borderNormal
            , detailPadding
            , UI.whenCss (not helpVisible) <|
                showHelpOnlyOnHover
            ]
        ]
        [ viewAccessInfo permalockReason cache relevantQuirks jutsu
        , UI.displayContents <|
            viewDetailShared cache jutsu resolutionOrder
        , UI.displayContents <|
            viewDetailSpecific cache jutsuById resolutionOrder jutsu.id jutsuSpecific
        , viewHelp cache helpVisible (extractTerms jutsuById (Jutsu jutsu jutsuSpecific))
        ]


viewDetailShared : DatabaseCache -> SharedPayload -> ResolutionOrder -> List (Html msg)
viewDetailShared cache payload resolutionOrder =
    [ viewBasicInfo cache payload
    , UI.BlockQuote.view payload.flavour
    , viewPrerekvizity cache resolutionOrder payload.prerekvizity
    , UI.displayContents <|
        JutsuEffect.viewPassive cache payload.pasivni
    ]


viewDetailSpecific :
    DatabaseCache
    -> JutsuById
    -> ResolutionOrder
    -> GUID GUID.Jutsu
    -> JutsuSpecific
    -> List (Html msg)
viewDetailSpecific cache jutsuById resolutionOrder thisJutsuId specific =
    case specific of
        Aktivni payload ->
            viewDetailAktivni cache payload resolutionOrder

        Pasivni ->
            []

        Vylepseni parentId ->
            viewVylepseniHeader cache parentId
                :: viewDetailVylepseni cache jutsuById resolutionOrder parentId


viewVylepseniHeader : DatabaseCache -> GUID GUID.Jutsu -> Html msg
viewVylepseniHeader cache parentId =
    DatabaseCache.viewSingleValueBuilder (Just parentId)
        |> DatabaseCache.withLabel "Nahrazuje techniku"
        |> DatabaseCache.withComment
            (DatabaseCache.treeNameFromJutsu cache (Just parentId))
        |> DatabaseCache.buildBlock cache


viewDetailVylepseni : DatabaseCache -> JutsuById -> ResolutionOrder -> GUID GUID.Jutsu -> List (Html msg)
viewDetailVylepseni cache jutsuById resolutionOrder parentId =
    case GUID.dictGet parentId jutsuById of
        Just (Jutsu _ (Aktivni payload)) ->
            viewDetailAktivni cache payload resolutionOrder

        Just (Jutsu _ (Vylepseni grandParentId)) ->
            viewDetailVylepseni cache jutsuById resolutionOrder grandParentId

        _ ->
            -- Debug.todo "TODO: vylepseni"
            [ Html.text "TODO: implementovat tuto cast" ]


viewDetailAktivni : DatabaseCache -> AktivniPayload -> ResolutionOrder -> List (Html msg)
viewDetailAktivni cache payload resolutionOrder =
    [ viewCena cache resolutionOrder payload
    , DatabaseCache.viewSingleValueBuilder payload.cooldown
        |> DatabaseCache.withLabel "Cooldown"
        |> DatabaseCache.buildInline cache
    , viewTyp cache resolutionOrder payload
    , viewActiveEffects cache resolutionOrder payload
    ]


viewAccessInfo : String -> DatabaseCache -> GUIDSet GUID.QuirkMechanicky -> SharedPayload -> Html msg
viewAccessInfo permalockReason cache relevantQuirks jutsu =
    Html.div
        [ Attrs.css
            [ Css.displayFlex
            , Css.flexDirection Css.column
            , Css.empty [ Css.display Css.none ]
            ]
        ]
        [ Html.text permalockReason
        , viewAccessDetail cache relevantQuirks jutsu
        ]


viewBasicInfo : DatabaseCache -> SharedPayload -> Html msg
viewBasicInfo cache jutsu =
    Html.div
        []
        [ Html.div
            [ Attrs.css
                [ Css.displayFlex
                , Css.flexWrap Css.wrap
                , Css.property "gap" "0.5rem"
                , Css.alignItems Css.baseline
                ]
            ]
            (Html.b [] [ Html.text jutsu.name ]
                :: List.map (wrapInParenthesis >> UI.gray) jutsu.alias
            )
        , viewMetadata cache jutsu
        ]


viewPrerekvizity : DatabaseCache -> ResolutionOrder -> GUIDSet GUID.Jutsu -> Html msg
viewPrerekvizity cache resolutionOrder idsRaw =
    let
        ids : GUIDSet GUID.Jutsu
        ids =
            GUID.setDiff idsRaw (GUID.setFromList (Utils.cons resolutionOrder))
    in
    if GUID.setLength ids == 0 then
        Html.text ""

    else
        let
            label : String
            label =
                if GUID.setLength ids == 1 then
                    "Naučení vyžaduje znalost techniky"

                else
                    "Naučení vyžaduje znalost technik"
        in
        UI.Definition.viewSimpleBlock label <|
            [ DatabaseCache.viewListBuilder "a" (GUID.setToList ids)
                |> DatabaseCache.buildInline cache

            -- TODO: zobrazit i strom (pokud je jinej)
            ]


viewAccessDetail : DatabaseCache -> GUIDSet GUID.QuirkMechanicky -> SharedPayload -> Html msg
viewAccessDetail cache relevantQuirks jutsu =
    case JutsuAccess.accessDetail cache relevantQuirks jutsu.access of
        Just accessDescription ->
            UI.minor accessDescription

        Nothing ->
            Html.text ""


viewTyp : DatabaseCache -> ResolutionOrder -> AktivniPayload -> Html msg
viewTyp cache resolutionOrder jutsu =
    let
        { old, new } =
            resolveVylepseniModifikator resolutionOrder jutsu.modifikator
    in
    UI.displayContents <|
        List.concat
            [ [ UI.Definition.stringLabeled "Typ techniky"
                    (DatabaseCache.name cache jutsu.typ)
              ]
            , old
                |> GUID.setToList
                -- TODO: sort
                |> List.map (viewModifikator cache False)
            , new
                |> GUID.setToList
                -- TODO: sort
                |> List.map (viewModifikator cache True)
            ]


viewModifikator : DatabaseCache -> Bool -> GUID GUID.Common -> Html msg
viewModifikator cache isNew modifikatorId =
    Html.div
        [ Attrs.css
            [ UI.Definition.indentBlock
            , UI.whenCss isNew <|
                UI.textHighlight
            ]
        ]
        [ DatabaseCache.viewDefinitionMinor cache modifikatorId
        ]


viewMetadata : DatabaseCache -> SharedPayload -> Html msg
viewMetadata cache jutsu =
    UI.row
        []
        [ UI.Definition.stringLabeledWithAlias "Úroveň techniky"
            (Just (Utils.levelToLetter jutsu.level))
            (Just (String.fromInt jutsu.level))
        , DatabaseCache.viewSingleValueBuilder (Just jutsu.tree)
            |> DatabaseCache.withLabel "Strom"
            |> DatabaseCache.withAlias True
            |> DatabaseCache.buildInline cache
        ]


viewActiveEffects : DatabaseCache -> ResolutionOrder -> AktivniPayload -> Html msg
viewActiveEffects cache resolutionOrder payload =
    let
        instant_ =
            instant resolutionOrder payload

        { old, new, merged } =
            payload.effects
                |> JutsuEffectVylepseni.resolveReplace [] resolutionOrder
    in
    if List.isEmpty merged then
        Html.text ""

    else
        UI.col [] <|
            List.concat
                [ [ viewMultiEffectHelp payload.typ
                        |> UI.whenHtml (List.length merged > 1)
                  ]
                , List.map (JutsuEffect.view cache resolutionOrder instant_) old
                , List.map (JutsuEffect.view cache resolutionOrder instant_) new
                    |> List.map (UI.wrapWithStyle UI.textHighlight)
                ]


viewMultiEffectHelp : GUID GUID.JutsuType -> Html msg
viewMultiEffectHelp typ =
    if GUID.equal GUID.const.efektPomocna typ then
        UI.little <|
            "Tuto techniku je možné seslat vícekrát. Konkrétní efekt lze vybrat až při využití."

    else if GUID.equal GUID.const.efektPasivni typ then
        Html.text ""

    else
        UI.little <|
            "Tuto techniku lze seslat pro jeden (pouze jeden) z následujících efektů."
                ++ " Konkrétní efekt je nutné vybrat již při seslání."


helpClassName : String
helpClassName =
    "elm--internal-cls--Jutsu-viewHelp-container"


showHelpOnlyOnHover : Css.Style
showHelpOnlyOnHover =
    Css.batch
        [ Css.Global.children
            [ Css.Global.class helpClassName
                [ Css.display Css.none
                ]
            ]
        , Css.hover
            [ Css.Global.children
                [ Css.Global.class helpClassName
                    [ Css.display Css.block
                    ]
                ]
            ]
        ]


viewHelp : DatabaseCache -> Bool -> GUIDSet a -> Html msg
viewHelp cache visible terms =
    let
        definedTerms : List (GUID a)
        definedTerms =
            List.filter (DatabaseCache.isDefined cache) (GUID.setToList terms)
    in
    if List.length definedTerms == 0 then
        Html.text ""

    else
        Html.div
            [ Attrs.css
                [ UI.textMinor
                , UI.whenCss (not visible) <|
                    Css.batch
                        [ UI.normal
                        , UI.textMinor
                        , UI.borderNormal
                        , detailPadding
                        , Css.borderTopWidth Css.zero
                        , Css.position Css.absolute
                        , Css.zIndex (Css.int 1)
                        , Css.top (Css.calc (Css.pct 100) Css.minus (Css.px 2))
                        , Css.property "left" ("-" ++ UI.borderNormalWidth)
                        , Css.property "right" ("-" ++ UI.borderNormalWidth)
                        , Css.boxShadow4 (Css.rem 0.1) (Css.rem 0.7) (Css.rem 0.5) (Css.hex "#000")
                        ]
                ]
            , Attrs.class helpClassName
            ]
            [ Html.hr
                [ Attrs.css
                    [ Css.property "border-color" UI.varColMinorFg
                    , Css.property "color" UI.varColMinorFg
                    ]
                ]
                []
            , Html.div
                [ Attrs.css
                    [ Css.displayFlex
                    , Css.flexDirection Css.column
                    , Css.property "gap" "3px"
                    , Css.alignItems Css.flexStart
                    ]
                ]
                (Html.span
                    [ Attrs.css
                        [ Css.alignSelf Css.center
                        , Css.fontStyle Css.italic
                        ]
                    ]
                    [ Html.text "Nápověda"
                    ]
                    :: List.map (DatabaseCache.viewDefinition cache) definedTerms
                )
            ]


viewCena : DatabaseCache -> ResolutionOrder -> AktivniPayload -> Html msg
viewCena cache resolutionOrder jutsu =
    if jutsu.typ == GUID.const.efektPasivni then
        Html.text ""

    else
        Html.div
            []
            [ UI.Definition.viewSimpleBlock "Sesílací cena"
                (viewCenaInner False cache resolutionOrder jutsu)
            ]


viewCenaInner : Bool -> DatabaseCache -> ResolutionOrder -> AktivniPayload -> List (Html msg)
viewCenaInner short cache resolutionOrder jutsu =
    let
        { old, new, prev, merged } =
            resolveVylepseniCena resolutionOrder jutsu.cena
    in
    List.concat
        [ DatabaseCache.viewValuesVsechny "" cache (GUID.dictToItems old)
        , DatabaseCache.viewValuesVsechny "" cache (GUID.dictToItems new)
            |> List.map (UI.wrapWithStyle UI.textHighlight)
        , [ UI.minor "(Zdarma)"
                |> UI.wrapWithStyleWhen UI.textHighlight (not (freeCena prev))
                |> UI.whenHtml (freeCena merged)
          , UI.minor "(Instatní/Reaktivní)"
                |> UI.wrapWithStyleWhen UI.textHighlight (not (instantCena prev))
                |> UI.whenHtml (instantCena merged && not short)
          ]
        ]



-- VIEW AS TABLE ROW


tableCellStyle : Css.Style
tableCellStyle =
    Css.batch
        [ Css.padding2 (Css.rem 0.2) (Css.rem 0.5)
        , Css.verticalAlign Css.top
        , Css.property "border" ("1.4px solid " ++ UI.varColFg)
        , Css.property "break-inside" "avoid"
        ]


verticalSep : Html msg
verticalSep =
    Html.div
        [ Attrs.css
            [ Css.property "border-left" ("2.4px solid " ++ UI.varColFg)
            , Css.marginRight (Css.rem 0.2)
            , Css.property "box-shadow" "0 0.2rem 0px 0px black, 0 -0.2rem 0px 0px black"
            ]
        ]
        []


viewTableContainer : List (Html msg) -> Html msg
viewTableContainer children =
    Html.table
        [ Attrs.css
            [ Css.borderCollapse Css.collapse
            , Css.boxSizing Css.borderBox
            , Css.maxWidth (Css.pct 100)
            ]
        ]
        children


viewTableHeader : DatabaseCache -> Maybe String -> GUID t -> Maybe (GUID GUID.Common) -> List (Html msg)
viewTableHeader cache color name_ pecet =
    [ Html.tr
        []
        [ Html.th
            [ Attrs.colspan (List.length columnNames)
            , Attrs.scope "col"
            , Attrs.css
                [ tableCellStyle
                , Css.borderTopWidth (Css.px 3)
                , Css.paddingTop (Css.rem 0.5)
                , UI.cssBgCol color
                ]
            ]
            [ DatabaseCache.viewName cache name_
            , UI.gray
                (DatabaseCache.nameAlias cache name_
                    |> Maybe.map (wrapInParenthesis >> (++) " ")
                    |> Maybe.withDefault ""
                )
            , Maybe.map GUID.toString_HACK pecet
                |> Maybe.map
                    (\x ->
                        Html.span
                            [ Attrs.css [ Css.fontWeight Css.normal, Css.marginLeft (Css.rem 2) ] ]
                            [ UI.minor "Poslední pečeť: ", Html.text x ]
                    )
                |> Maybe.withDefault (Html.text "")
            ]
        ]
    , Html.tr
        []
        (List.map (viewTableContainerTH color) columnNames)
    ]


viewTableContainerTH : Maybe String -> String -> Html msg
viewTableContainerTH color colName =
    Html.th
        [ Attrs.scope "col"
        , Attrs.css
            [ tableCellStyle
            , Css.whiteSpace Css.noWrap
            , UI.cssBgCol color
            ]
        ]
        [ UI.little colName
        ]


columnNames : List String
columnNames =
    List.map Tuple.first columnDef


columnVals : List (ColumnDef msg)
columnVals =
    List.map Tuple.second columnDef


type alias ColumnDef msg =
    ColDefParams -> Html msg


type alias ColDefParams =
    { j : Jutsu
    , jp : SharedPayload
    , c : DatabaseCache
    , resolutionOrder : ResolutionOrder
    }


type alias ColDefParamsAktivni =
    { j : Jutsu
    , jp : SharedPayload
    , c : DatabaseCache
    , a : AktivniPayload
    , resolutionOrder : ResolutionOrder
    }


columnShared : String -> ColumnDef msg -> ( String, ColumnDef msg )
columnShared colName def =
    ( colName, def )


columnAktivni : String -> (ColDefParamsAktivni -> Html msg) -> ( String, ColumnDef msg )
columnAktivni colName def =
    ( colName
    , \{ j, jp, c, resolutionOrder } ->
        case j of
            Jutsu _ (Aktivni a) ->
                def { j = j, jp = jp, c = c, a = a, resolutionOrder = resolutionOrder }

            _ ->
                Html.text ""
    )


columnDef : List ( String, ColumnDef msg )
columnDef =
    let
        m : Maybe String -> String
        m =
            Maybe.withDefault ""

        row : List (Html msg) -> Html msg
        row =
            Html.div
                [ Attrs.css
                    [ Css.displayFlex
                    , Css.property "gap" "0 0.5rem"
                    , Css.flexWrap Css.wrap
                    , Css.alignItems Css.baseline
                    ]
                ]

        rowSep : List (Html msg) -> Html msg
        rowSep children =
            Html.div
                [ Attrs.css
                    [ Css.displayFlex
                    , Css.property "gap" "0 0.5rem"
                    ]
                ]
                (List.intersperse verticalSep children)
    in
    [ columnShared "Úroveň" <|
        \{ jp } ->
            Html.text
                (Utils.levelToLetter jp.level
                    ++ " "
                    ++ wrapInParenthesis (String.fromInt jp.level)
                )
    , columnShared "Jméno" <|
        \{ jp } ->
            row
                [ Html.text jp.name
                , UI.minor (m (Maybe.map wrapInParenthesis (List.head jp.alias)))
                ]
    , columnAktivni "Typ" <|
        \{ c, a, resolutionOrder } ->
            let
                { new, old } =
                    resolveVylepseniModifikator resolutionOrder a.modifikator
            in
            row
                [ Html.text (m (DatabaseCache.name c a.typ))
                , List.concat
                    [ old
                        |> GUID.setToList
                        |> List.filterMap (DatabaseCache.name c)
                        |> List.map Html.text
                    , new
                        |> GUID.setToList
                        |> List.filterMap (DatabaseCache.name c)
                        |> List.map UI.highlight
                    ]
                    |> List.intersperse (Html.text ", ")
                    |> wrapInParenthesisInline
                ]
    , columnShared "Vyžaduje" <|
        \{ c, jp } ->
            GUID.setToList jp.prerekvizity
                |> List.filterMap (DatabaseCache.name c)
                |> String.join ", "
                |> Html.text
    , columnShared "Pasivně" <|
        \{ c, j } ->
            row (DatabaseCache.viewValuesAlias "+" c (givenAttributes j))
    , columnShared "Pasivně sleva" <|
        \{ c, jp } ->
            row (JutsuEffect.viewSlevaShort c jp.pasivni)
    , columnAktivni "Cooldown" <|
        \{ c, a } ->
            Html.text (m (Maybe.andThen (DatabaseCache.nameAlias c) a.cooldown))
    , columnAktivni "Cena" <|
        \{ c, a, resolutionOrder } ->
            viewCenaInner True c resolutionOrder a
                |> row
    , columnAktivni "Efekt" <|
        \{ c, a, resolutionOrder } ->
            let
                { old, new } =
                    JutsuEffectVylepseni.resolveReplace [] resolutionOrder a.effects

                viewSingleJutsu : Effect -> Html msg
                viewSingleJutsu =
                    JutsuEffect.viewShort c resolutionOrder (instant resolutionOrder a)
            in
            rowSep <|
                List.concat
                    [ List.map viewSingleJutsu old
                    , List.map (viewSingleJutsu >> UI.wrapWithStyle UI.textHighlight) new
                    ]
    ]


wrapTableCell : Maybe String -> Html msg -> Html msg
wrapTableCell color children =
    Html.td
        [ Attrs.css
            [ tableCellStyle
            , UI.cssBgCol color
            ]
        ]
        [ children ]


viewTableRow : DatabaseCache -> JutsuById -> Maybe String -> Jutsu -> Html msg
viewTableRow cache jutsuById color (Jutsu jp js) =
    let
        ( Jutsu _ baseSpecific, resolutionOrder ) =
            vylepseniResolutionOrder jutsuById (Jutsu jp js)

        params : ColDefParams
        params =
            { j = Jutsu jp baseSpecific, jp = jp, c = cache, resolutionOrder = resolutionOrder }

        view : ColumnDef msg -> Html msg
        view v =
            wrapTableCell color
                (v params)
    in
    Html.tr
        []
        (List.map view columnVals)
