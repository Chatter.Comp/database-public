module Utils exposing (..)

import Array
import Css
import Env.Env
import Env.Util.BuildVariables
import Html.Styled as Html exposing (Html)
import Html.Styled.Attributes as Attrs
import Html.Styled.Events as Events
import List.Extra
import UI


justIfNotEmptyString : String -> Maybe String
justIfNotEmptyString s =
    case s of
        "" ->
            Nothing

        notEmpty ->
            Just notEmpty


boolToInt : Bool -> Int
boolToInt bool =
    if bool then
        1

    else
        0


if_ : Bool -> a -> a -> a
if_ predicate true false =
    if predicate then
        true

    else
        false



-- NON-EMPTY LIST


type alias ListNotEmpty a =
    ( a, List a )


{-| If not found, fallback to the first element (head)
-}
alwaysFind : (a -> Bool) -> ListNotEmpty a -> a
alwaysFind predicate ( head, rest ) =
    List.Extra.find predicate rest
        |> Maybe.withDefault head


alwaysSort : (a -> comparable) -> ListNotEmpty a -> ListNotEmpty a
alwaysSort key ( head, rest ) =
    List.sortBy key (head :: rest)
        |> listNotEmpty
        |> Maybe.withDefault ( head, [] )


cons : ListNotEmpty a -> List a
cons ( head, rest ) =
    head :: rest


listNotEmpty : List a -> Maybe (ListNotEmpty a)
listNotEmpty list =
    case list of
        head :: tail ->
            Just ( head, tail )

        [] ->
            Nothing



-- NARUTO


levelToLetter : Int -> String
levelToLetter lvl =
    let
        levelLetters : Array.Array String
        levelLetters =
            Array.fromList [ "F", "E", "D", "C", "B", "A", "S", "S+", "S+", "S+" ]
    in
    Array.get lvl levelLetters
        |> Maybe.withDefault ""


type Vsechny a
    = Vsechny
    | Prave a



-- INFRASTRUCTURE


versionText : Html msg
versionText =
    Html.span
        []
        [ UI.minor "Verze: "
        , Html.text <|
            String.concat
                [ Env.Util.BuildVariables.version
                , " ("
                , Env.Util.BuildVariables.versionDate
                , ")"
                ]
        ]


testRibbon : Html msg
testRibbon =
    if Env.Env.environment.showTestRibbon then
        Html.div
            [ Attrs.css
                [ Css.position Css.fixed
                , Css.zIndex (Css.int 2)
                , Css.top Css.zero
                , Css.right (Css.rem 0.2)
                , Css.padding (Css.rem 0.2)
                , Css.paddingBottom (Css.rem 0.5)
                , Css.fontWeight Css.bold
                , Css.color (Css.hex "#fff")
                , Css.backgroundColor (Css.hex "#ff7a00")
                , Css.property "clip-path" "polygon(0 0, 100% 0, 100% calc(100% - 0.5rem), 50% 100%, 0 calc(100% - 0.5rem));"
                ]
            , Attrs.title "Toto je testovací prostředí, a obsahuje rozpracované změny. Pro oficiální verzi pravidel jdi na narutolarp.cz"
            ]
            [ Html.text "TEST"
            ]

    else
        Html.text ""
