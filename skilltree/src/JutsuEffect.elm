module JutsuEffect exposing
    ( Atribut
    , Effect
    , Pasivni
    , SlevaPayload
    , TechnikaSelector
    , effectsDecoder
    , extractPasivniTerms
    , extractTerms
    , givenAttributes
    , pasivniDecoder
    , view
    , viewPassive
    , viewShort
    , viewSlevaShort
    )

import Css
import DatabaseCache exposing (DatabaseCache)
import GUID exposing (GUID, GUIDSet)
import Html.Styled as Html exposing (Html)
import Html.Styled.Attributes as Attrs
import JutsuEffectVylepseni exposing (ResolutionOrder)
import List.Extra
import UI
import UI.Definition
import Utils exposing (Vsechny(..))
import Xml.Decode


givenAttributes : Pasivni -> List Atribut
givenAttributes (Pasivni attrs _) =
    attrs


extractPasivniTerms : Pasivni -> GUIDSet GUID.Common
extractPasivniTerms (Pasivni _ sleva) =
    sleva
        |> Maybe.map (.cena >> List.map Tuple.first)
        |> Maybe.withDefault []
        |> GUID.setFromList


extractTerms : Effect -> GUIDSet GUID.Common
extractTerms (Effect payload effect) =
    GUID.setEmpty
        |> extractTermsShared payload
        |> extractTermsInner effect


extractTermsInner : InnerEffect -> GUIDSet GUID.Common -> GUIDSet GUID.Common
extractTermsInner effect =
    case effect of
        UtocnaPridej { attack, req } podminky ->
            extractZasah attack
                >> Maybe.withDefault identity (Maybe.map extractZasah req)
                >> GUID.setUnion (GUID.setFromList podminky.reqHesla)

        Utocna { attack, req } ->
            extractZasah attack
                >> Maybe.withDefault identity (Maybe.map extractZasah req)

        Obranna zasah stinZivot ->
            extractZasah zasah
                >> GUID.setAddIf GUID.const.stinZivot stinZivot

        Leceni _ ->
            GUID.setAdd GUID.const.leceni

        Neviditelnost _ ->
            GUID.setAdd GUID.const.skryti

        Vnimani _ ->
            GUID.setAdd GUID.const.vnimani

        Specialni ->
            identity


extractTermsShared : EffectPayload -> GUIDSet GUID.Common -> GUIDSet GUID.Common
extractTermsShared shared =
    GUID.setUnion (GUID.setFromList shared.efektNaSesilatele.hesla)


extractZasah : ZasahPayload -> GUIDSet GUID.Common -> GUIDSet GUID.Common
extractZasah zasah =
    GUID.setUnion (GUID.setFromList (List.map GUID.toCommon zasah.heslo))
        >> GUID.setUnion (GUID.setFromList (List.map GUID.toCommon zasah.zasah))



-- POMOCNE TYPY


type alias ZasahPayload =
    { zasah : List (GUID GUID.Zasah)
    , heslo : List (GUID GUID.Heslo)
    , dmg : JutsuEffectVylepseni.Group Int
    }


payloadDecoderZasah : GUID GUID.Jutsu -> Xml.Decode.Decoder ZasahPayload
payloadDecoderZasah rootJutsuId =
    Xml.Decode.map3 ZasahPayload
        (GUID.refListPathDecoder [ "zásah" ])
        (GUID.refListPathDecoder [ "heslo" ])
        (JutsuEffectVylepseni.decoderSingle [ "zranění" ] Xml.Decode.int rootJutsuId)


type alias PodminkyPayload =
    { reqHesla : List (GUID GUID.Common)
    , reqDmg : Int
    }


payloadDecoderPodminky : Xml.Decode.Decoder PodminkyPayload
payloadDecoderPodminky =
    Xml.Decode.map2 PodminkyPayload
        (GUID.refListPathDecoder [ "podmínky", "heslo" ])
        (Xml.Decode.optionalPath [ "podmínky", "zranění" ] (Xml.Decode.single Xml.Decode.int) 0 (Xml.Decode.succeed identity))


type alias EfektNaSesilatelePayload =
    { hesla : List (GUID GUID.Common)
    , dmg : Int
    }


payloadDecoderEfektNaSesilatele : Xml.Decode.Decoder EfektNaSesilatelePayload
payloadDecoderEfektNaSesilatele =
    Xml.Decode.map2 EfektNaSesilatelePayload
        (GUID.refListPathDecoder [ "efekt-na-sesilatele", "heslo" ])
        (Xml.Decode.optionalPath [ "efekt-na-sesilatele", "zranění" ] (Xml.Decode.single Xml.Decode.int) 0 (Xml.Decode.succeed identity))


type alias LeceniPayload =
    { hesla : Vsechny (List (GUID GUID.Common))
    , dmg : Vsechny Int
    }


payloadDecoderLeceni : Xml.Decode.Decoder LeceniPayload
payloadDecoderLeceni =
    Xml.Decode.map2 LeceniPayload
        (Xml.Decode.oneOf
            [ Xml.Decode.path [ "všechny-hesla" ] (Xml.Decode.single (Xml.Decode.succeed Vsechny))
            , GUID.refListPathDecoder [ "heslo" ]
                |> Xml.Decode.map Prave
            ]
        )
        (Xml.Decode.oneOf
            [ Xml.Decode.path [ "všechny-zranění" ] (Xml.Decode.single (Xml.Decode.succeed Vsechny))
            , Xml.Decode.optionalPath [ "zranění" ] (Xml.Decode.single Xml.Decode.int) 0 (Xml.Decode.succeed identity)
                |> Xml.Decode.map Prave
            ]
        )


type alias VnimaniPayload =
    { typ : GUID GUID.Common
    , lvl : JutsuEffectVylepseni.Group Int
    }


payloadDecoderVnimani : GUID GUID.Jutsu -> Xml.Decode.Decoder VnimaniPayload
payloadDecoderVnimani rootJutsuId =
    Xml.Decode.map2 VnimaniPayload
        (Xml.Decode.path [ "typ-vnímání" ] (Xml.Decode.single GUID.refDecoder))
        (JutsuEffectVylepseni.decoderSingle [ "úroveň" ] Xml.Decode.int rootJutsuId)


type TechnikaSelector
    = SelectVsechnyTechniky
    | SelectStrom (GUIDSet GUID.Tree)
    | SelectTechnika (GUIDSet GUID.Jutsu)


type alias SlevaPayload =
    { selector : TechnikaSelector
    , cena : List ( GUID GUID.Common, Int )
    }


slevaDecoder : Xml.Decode.Decoder SlevaPayload
slevaDecoder =
    let
        selectorDecoder : Xml.Decode.Decoder TechnikaSelector
        selectorDecoder =
            Xml.Decode.oneOf
                [ Xml.Decode.path [ "sleva", "všechny-techniky" ] (Xml.Decode.single (Xml.Decode.succeed SelectVsechnyTechniky))
                , GUID.refListPathDecoderNonEmpty [ "sleva", "strom" ]
                    |> Xml.Decode.map (SelectStrom << GUID.setFromList)
                , GUID.refListPathDecoderNonEmpty [ "sleva", "technika" ]
                    |> Xml.Decode.map (SelectTechnika << GUID.setFromList)
                ]
    in
    Xml.Decode.map2 SlevaPayload
        selectorDecoder
        (Xml.Decode.path [ "sleva", "cena" ] (Xml.Decode.list GUID.refIntDecoder))



-- EFFECT


type alias EffectPayload =
    { specialni : JutsuEffectVylepseni.Group String
    , vylepsujeTechniku : Maybe (GUID GUID.Jutsu)
    , efektNaSesilatele : EfektNaSesilatelePayload
    , zdvojitCenu : Bool
    }


type Effect
    = Effect EffectPayload InnerEffect


type alias Atribut =
    ( GUID GUID.Atribut, Int )


type alias AttackPayload =
    { attack : ZasahPayload
    , req : Maybe ZasahPayload
    }


type InnerEffect
    = UtocnaPridej AttackPayload PodminkyPayload
    | Utocna AttackPayload
    | Obranna ZasahPayload Bool
    | Leceni LeceniPayload
    | Neviditelnost (JutsuEffectVylepseni.Group Int)
    | Vnimani VnimaniPayload
    | Specialni


type Pasivni
    = Pasivni (List Atribut) (Maybe SlevaPayload)


pasivniDecoder : Xml.Decode.Decoder Pasivni
pasivniDecoder =
    Xml.Decode.path [ "pasivní" ] (Xml.Decode.single pasivniPayloadDecoder)
        |> Xml.Decode.maybe
        |> Xml.Decode.map (Maybe.withDefault (Pasivni [] Nothing))


pasivniPayloadDecoder : Xml.Decode.Decoder Pasivni
pasivniPayloadDecoder =
    Xml.Decode.map2 Pasivni
        (Xml.Decode.path [ "atribut" ] (Xml.Decode.list GUID.refIntDecoder))
        (Xml.Decode.maybe slevaDecoder)


effectsWrapDecoder : GUID GUID.Jutsu -> Xml.Decode.Decoder InnerEffect -> Xml.Decode.Decoder Effect
effectsWrapDecoder rootJutsuId =
    Xml.Decode.map2 Effect
        (effectPayloadDecoder rootJutsuId)


effectPayloadDecoder : GUID GUID.Jutsu -> Xml.Decode.Decoder EffectPayload
effectPayloadDecoder rootJutsuId =
    Xml.Decode.map4 EffectPayload
        (JutsuEffectVylepseni.decoderSingle [ "speciální" ] Xml.Decode.string rootJutsuId)
        -- (Xml.Decode.optionalPath [ "speciální" ] (Xml.Decode.single Xml.Decode.string) "" (Xml.Decode.succeed identity))
        (Xml.Decode.maybe <| Xml.Decode.path [ "vylepšuje-techniku" ] (Xml.Decode.single GUID.refDecoder))
        payloadDecoderEfektNaSesilatele
        (Xml.Decode.optionalPath [ "zdvojit-cenu" ] (Xml.Decode.single (Xml.Decode.succeed True)) False (Xml.Decode.succeed identity))


effectsDecoder : GUID GUID.Jutsu -> Xml.Decode.Decoder (JutsuEffectVylepseni.Group (List Effect))
effectsDecoder rootJutsuId =
    Xml.Decode.map2 (JutsuEffectVylepseni.merge List.append)
        (JutsuEffectVylepseni.decoder [ "útočná-přidej" ] (effectDecoderUtocnaPridej rootJutsuId) rootJutsuId)
        (JutsuEffectVylepseni.decoder [ "útočná" ] (effectDecoderUtocna rootJutsuId) rootJutsuId)
        |> Xml.Decode.map2 (JutsuEffectVylepseni.merge List.append)
            (JutsuEffectVylepseni.decoder [ "obranná" ] (effectDecoderObranna rootJutsuId) rootJutsuId)
        |> Xml.Decode.map2 (JutsuEffectVylepseni.merge List.append)
            (JutsuEffectVylepseni.decoder [ "léčení" ] (effectDecoderLeceni rootJutsuId) rootJutsuId)
        |> Xml.Decode.map2 (JutsuEffectVylepseni.merge List.append)
            (JutsuEffectVylepseni.decoder [ "neviditelnost" ] (effectDecoderNeviditelnost rootJutsuId) rootJutsuId)
        |> Xml.Decode.map2 (JutsuEffectVylepseni.merge List.append)
            (JutsuEffectVylepseni.decoder [ "vnímání" ] (efectDecoderVnimani rootJutsuId) rootJutsuId)
        |> Xml.Decode.map2 (JutsuEffectVylepseni.merge List.append)
            (JutsuEffectVylepseni.decoder [ "speciální" ] (effectsWrapDecoder rootJutsuId <| Xml.Decode.succeed Specialni) rootJutsuId)


effectDecoderNeviditelnost : GUID GUID.Jutsu -> Xml.Decode.Decoder Effect
effectDecoderNeviditelnost rootJutsuId =
    effectsWrapDecoder rootJutsuId <|
        Xml.Decode.map Neviditelnost
            (JutsuEffectVylepseni.decoderSingle [ "úroveň" ] Xml.Decode.int rootJutsuId)


efectDecoderVnimani : GUID GUID.Jutsu -> Xml.Decode.Decoder Effect
efectDecoderVnimani rootJutsuId =
    effectsWrapDecoder rootJutsuId <|
        Xml.Decode.map Vnimani
            (payloadDecoderVnimani rootJutsuId)


effectDecoderUtocnaPridej : GUID GUID.Jutsu -> Xml.Decode.Decoder Effect
effectDecoderUtocnaPridej rootJutsuId =
    effectsWrapDecoder rootJutsuId <|
        Xml.Decode.map2 UtocnaPridej
            (attackWithFollowupDecoder rootJutsuId)
            payloadDecoderPodminky


effectDecoderUtocna : GUID GUID.Jutsu -> Xml.Decode.Decoder Effect
effectDecoderUtocna rootJutsuId =
    effectsWrapDecoder rootJutsuId <|
        Xml.Decode.map Utocna <|
            attackWithFollowupDecoder rootJutsuId


attackWithFollowupDecoder : GUID GUID.Jutsu -> Xml.Decode.Decoder AttackPayload
attackWithFollowupDecoder rootJutsuId =
    Xml.Decode.map2 AttackPayload
        (payloadDecoderZasah rootJutsuId)
        (Xml.Decode.possiblePath [ "musí-následovat-zásah" ]
            (Xml.Decode.single (payloadDecoderZasah rootJutsuId))
            (Xml.Decode.succeed identity)
        )


effectDecoderObranna : GUID GUID.Jutsu -> Xml.Decode.Decoder Effect
effectDecoderObranna rootJutsuId =
    effectsWrapDecoder rootJutsuId <|
        Xml.Decode.map2 Obranna
            (payloadDecoderZasah rootJutsuId)
            (Xml.Decode.possiblePath [ "stínový-život" ]
                (Xml.Decode.single (Xml.Decode.succeed True))
                (Xml.Decode.succeed (Maybe.withDefault False))
            )


effectDecoderLeceni : GUID GUID.Jutsu -> Xml.Decode.Decoder Effect
effectDecoderLeceni rootJutsuId =
    effectsWrapDecoder rootJutsuId <|
        Xml.Decode.map Leceni
            payloadDecoderLeceni



-- VIEW


viewShort : DatabaseCache -> ResolutionOrder -> Bool -> Effect -> Html msg
viewShort =
    viewInner True


view : DatabaseCache -> ResolutionOrder -> Bool -> Effect -> Html msg
view =
    viewInner False


spacedColumnStyle : List Css.Style
spacedColumnStyle =
    [ Css.displayFlex
    , Css.flexDirection Css.column
    , Css.property "gap" "0.4rem"
    ]


viewInner : Bool -> DatabaseCache -> ResolutionOrder -> Bool -> Effect -> Html msg
viewInner short cache resolutionOrder instant (Effect shared effect) =
    viewEffectHeader
        short
        instant
        shared.zdvojitCenu
        (effectMod effect)
        (List.concat
            [ [ DatabaseCache.viewSingleValueBuilder shared.vylepsujeTechniku
                    |> DatabaseCache.withLabel "Vylepšuje techniku"
                    |> DatabaseCache.withComment
                        (DatabaseCache.treeNameFromJutsu cache shared.vylepsujeTechniku)
                    |> DatabaseCache.buildInline cache
              ]
            , viewMainEffect short cache resolutionOrder effect
            , [ viewEfektNaSesilatele short cache shared.efektNaSesilatele
              , viewSpecialni resolutionOrder shared.specialni
              ]
            ]
        )


viewSpecialni : ResolutionOrder -> JutsuEffectVylepseni.Group String -> Html msg
viewSpecialni resolutionOrder specialni =
    let
        { prev, merged } =
            JutsuEffectVylepseni.resolveReplace "" resolutionOrder specialni
    in
    Html.text merged
        |> UI.wrapWithStyleWhen UI.textHighlight (prev /= merged)
        |> UI.whenHtml (merged /= "")


effectMod : InnerEffect -> Maybe String
effectMod _ =
    -- case effect of
    --     Utocna { req } ->
    --         req
    --             |> Maybe.map (always "Kombo")
    --     Kombo { req } _ ->
    --         req
    --             |> Maybe.map (always "Kombo")
    --     _ ->
    Nothing


viewEfektNaSesilatele : Bool -> DatabaseCache -> EfektNaSesilatelePayload -> Html msg
viewEfektNaSesilatele short cache efektNaSesilatele =
    DatabaseCache.viewListBuilder "a" efektNaSesilatele.hesla
        |> DatabaseCache.withSuffix (dmgToSuffixResolved efektNaSesilatele.dmg)
        |> DatabaseCache.withShort short
        |> DatabaseCache.withLabel
            (if short then
                "Sesilatel utrpí:"

             else
                "Po provedení tohoto efektu sesilatel utrpí:"
            )
        |> DatabaseCache.withComment
            (if efektNaSesilatele.dmg > 0 then
                Just "Nelze použít úhyb ani redukci"

             else
                Just "Nelze použít úhyb"
            )
        |> DatabaseCache.buildBlock cache


viewEffectHeader : Bool -> Bool -> Bool -> Maybe String -> List (Html msg) -> Html msg
viewEffectHeader short instant zdvojitCenu effectModificator children =
    Html.div
        [ Attrs.css
            [ Css.property "border-top" ("1px solid " ++ UI.varColMinorFg)
            , Css.padding (Css.rem 0.2)
            , Css.paddingTop (Css.rem 0.6)
            , Css.marginTop (Css.rem 0.6)
            , Css.position Css.relative
            , Css.batch spacedColumnStyle
            ]
            |> UI.whenAttr (not short)
        ]
        ((UI.whenHtml (not short) <|
            Html.span
                [ Attrs.css
                    [ Css.position Css.absolute
                    , Css.top (Css.rem -0.8)
                    , Css.left (Css.rem 1)
                    , Css.padding (Css.rem 0.2)
                    , Css.property "background-color" UI.varColBg
                    , UI.textLittle
                    ]
                ]
                [ Html.text "Efekt"
                , case effectModificator of
                    Just mod ->
                        Html.text (" (" ++ mod ++ ")")

                    Nothing ->
                        Html.text ""
                ]
         )
            :: UI.whenHtml zdvojitCenu
                (Html.div
                    []
                    [ Html.text "Tento efekt má dvojnásobnou sesílací cenu"
                    , Html.text " (né pečetě)"
                        |> UI.whenHtml (not instant)
                    ]
                )
            :: children
        )


wrapIf : Bool -> List (Html msg) -> Html msg
wrapIf cond =
    Html.div
        [ Attrs.css
            [ Css.displayFlex
            , Css.flexWrap Css.wrap
            , Css.property "gap" "0 0.5rem"
            ]
            |> UI.whenAttr cond
        ]


viewMainEffect : Bool -> DatabaseCache -> ResolutionOrder -> InnerEffect -> List (Html msg)
viewMainEffect short cache resolutionOrder effect =
    case effect of
        Utocna payload ->
            viewUtocna short cache resolutionOrder payload

        UtocnaPridej attack podminky ->
            viewUtocnaPridej short cache resolutionOrder attack podminky

        Obranna zasah stinZivot ->
            viewObranna short cache resolutionOrder zasah stinZivot

        Leceni x ->
            viewLeceni cache x

        Neviditelnost x ->
            viewNeviditelnost resolutionOrder x

        Vnimani x ->
            viewVnimani cache resolutionOrder x

        Specialni ->
            []


viewVnimani : DatabaseCache -> ResolutionOrder -> VnimaniPayload -> List (Html msg)
viewVnimani cache resolutionOrder payload =
    let
        { prev, merged } =
            JutsuEffectVylepseni.resolveReplace 0 resolutionOrder payload.lvl
    in
    [ Html.div
        []
        [ UI.Definition.viewLabel "Vnímání"
        , DatabaseCache.viewName cache payload.typ
        , Html.text " "
        , UI.Definition.viewName (String.fromInt merged)
            |> UI.wrapWithStyleWhen UI.textHighlight (prev /= merged)
        ]
    ]


viewNeviditelnost : ResolutionOrder -> JutsuEffectVylepseni.Group Int -> List (Html msg)
viewNeviditelnost resolutionOrder lvl =
    let
        { prev, merged } =
            JutsuEffectVylepseni.resolveReplace 0 resolutionOrder lvl
    in
    [ Html.div
        []
        [ UI.Definition.viewName "Skrytí"
        , Html.text " "
        , UI.Definition.viewName (String.fromInt merged)
            |> UI.wrapWithStyleWhen UI.textHighlight (prev /= merged)
        ]
    ]


viewLeceni : DatabaseCache -> LeceniPayload -> List (Html msg)
viewLeceni cache arg1 =
    let
        dmg : List String
        dmg =
            case arg1.dmg of
                Prave dmgValue ->
                    dmgToSuffixResolved dmgValue

                Vsechny ->
                    [ "Všechna zranění" ]
    in
    case arg1.hesla of
        Prave hesla ->
            [ DatabaseCache.viewListBuilder "a" hesla
                |> DatabaseCache.withLabel "Léčení"
                |> DatabaseCache.withSuffix dmg
                |> DatabaseCache.buildInline cache
            ]

        Vsechny ->
            [ UI.Definition.stringListLabeled "a" "Léčení" ("Všechna hesla" :: dmg)
            ]


viewObranna : Bool -> DatabaseCache -> ResolutionOrder -> ZasahPayload -> Bool -> List (Html msg)
viewObranna short cache resolutionOrder arg1 stinZivot =
    if stinZivot then
        []
        --Debug.todo "remove"

    else
        [ wrapIf short
            [ DatabaseCache.viewListBuilder "nebo" arg1.zasah
                |> DatabaseCache.withLabel "Když dostaneš útok typu"
                |> DatabaseCache.buildInline cache
            , DatabaseCache.viewListBuilder "," arg1.heslo
                |> DatabaseCache.withLabel "Zahlaš útočníkovi"
                |> DatabaseCache.withSuffixManual (dmgToSuffix resolutionOrder arg1.dmg ++ counterAttackType cache resolutionOrder arg1)
                |> DatabaseCache.buildInline cache

            -- TODO: rozlisit defensivni a ofensivni cast (+dat vzdy ofensivni jen nablizko)
            ]
        ]


counterAttackType : DatabaseCache -> ResolutionOrder -> ZasahPayload -> List (Html Never)
counterAttackType cache resolutionOrder zasah =
    let
        dmg =
            JutsuEffectVylepseni.resolveReplace 0 resolutionOrder zasah.dmg
    in
    if List.all (DatabaseCache.isDefensive cache) zasah.heslo && dmg.merged == 0 then
        []

    else
        [ "Pohledem", "Najisto" ]
            |> List.map UI.Definition.viewName


dmgToSuffix : ResolutionOrder -> JutsuEffectVylepseni.Group Int -> List (Html Never)
dmgToSuffix resolutionOrder dmg =
    let
        { prev, merged } =
            JutsuEffectVylepseni.resolveReplace 0 resolutionOrder dmg
    in
    if merged == 0 then
        []

    else
        [ UI.Definition.viewName (String.fromInt merged ++ " zranění")
            |> UI.wrapWithStyleWhen UI.textHighlight (prev /= merged)
        ]


dmgToSuffixResolved : Int -> List String
dmgToSuffixResolved dmg =
    if dmg == 0 then
        []

    else
        [ String.fromInt dmg ++ " zranění" ]


viewUtocnaPridej : Bool -> DatabaseCache -> ResolutionOrder -> AttackPayload -> PodminkyPayload -> List (Html msg)
viewUtocnaPridej short cache resolutionOrder { attack, req } podminky =
    [ Html.div
        []
        [ wrapIf short
            [ DatabaseCache.viewListBuilder "nebo" attack.zasah
                |> DatabaseCache.withLabel "K útoku"
                |> DatabaseCache.buildInline cache
            , DatabaseCache.viewListBuilder "a" podminky.reqHesla
                |> DatabaseCache.withLabel "který už dává alespoň"
                |> DatabaseCache.withSuffix (dmgToSuffixResolved podminky.reqDmg)
                |> DatabaseCache.buildInline cache
            , DatabaseCache.viewListBuilder "a" attack.heslo
                |> DatabaseCache.withLabel "přidej"
                |> DatabaseCache.withSuffixManual (dmgToSuffix resolutionOrder attack.dmg)
                |> DatabaseCache.buildInline cache
            ]
        , viewUtocnaPridejHint cache short attack.zasah
        ]
    , viewZasahReq short cache resolutionOrder req
    ]


viewUtocnaPridejHint : DatabaseCache -> Bool -> List (GUID GUID.Zasah) -> Html msg
viewUtocnaPridejHint cache short zasah =
    if short || List.length zasah == 0 then
        Html.text ""

    else
        let
            allowZakladniUtokLength : Int
            allowZakladniUtokLength =
                List.Extra.count (DatabaseCache.allowZakladniUtok cache) zasah
        in
        if allowZakladniUtokLength == List.length zasah then
            UI.minor "(Lze přidat k základnímu útoku zbraní/rukou, v takovém případě nezapomeň přičíst 1 zranění ze základního útoku. Nebo přidat k technice která říká 'Proveď útok'.)"

        else if allowZakladniUtokLength == 0 then
            UI.minor "(Je nutné přidat k technice která říká 'Proveď útok')"

        else
            -- TODO: mozna vypsat konkretni typy zasahu?
            UI.minor "(Lze přidat k základnímu útoku zbraní/rukou, v takovém případě nezapomeň přičíst 1 zranění ze základního útoku. Nebo přidat k technice která říká 'Proveď útok'.)"


viewUtocna : Bool -> DatabaseCache -> ResolutionOrder -> AttackPayload -> List (Html msg)
viewUtocna short cache resolutionOrder { attack, req } =
    let
        attackDmg =
            JutsuEffectVylepseni.resolveReplace 0 resolutionOrder attack.dmg
    in
    [ Html.div
        []
        [ wrapIf short
            [ DatabaseCache.viewListBuilder "nebo" attack.zasah
                |> DatabaseCache.withLabel "Proveď útok"
                |> DatabaseCache.buildInline cache
            , DatabaseCache.viewListBuilder "a" attack.heslo
                |> DatabaseCache.withLabel "který způsobí"
                |> DatabaseCache.withSuffixManual (dmgToSuffix resolutionOrder attack.dmg)
                |> DatabaseCache.buildInline cache
            ]
        , UI.whenHtml (attackDmg.merged == 0 && not short) <|
            UI.minor "(Tento útok sám o sobě nezpůsobuje žádné zranění)"
        ]
    , viewZasahReq short cache resolutionOrder req
    ]


viewZasahReq : Bool -> DatabaseCache -> ResolutionOrder -> Maybe ZasahPayload -> Html msg
viewZasahReq short cache resolutionOrder req =
    case req of
        Nothing ->
            Html.text ""

        Just reqZasah ->
            let
                core : List (Html msg)
                core =
                    [ DatabaseCache.viewListBuilder "nebo" reqZasah.zasah
                        |> DatabaseCache.withLabel "zásah typu"
                        |> DatabaseCache.buildInline cache
                    , DatabaseCache.viewListBuilder "a" reqZasah.heslo
                        |> DatabaseCache.withLabel "který měl efekt alespoň"
                        |> DatabaseCache.withSuffixManual (dmgToSuffix resolutionOrder reqZasah.dmg)
                        |> DatabaseCache.buildInline cache
                    ]
            in
            if short then
                UI.Definition.viewSimpleBlockShort "Vyžaduje:"
                    [ wrapIf True
                        core
                    ]

            else
                UI.Definition.viewSimpleBlock "Tento útok lze použít pouze proti cíli kterému jsi bezprostředně předtím udělil:"
                    [ Html.div [] core
                    , UI.minor "(Tento zásah nemusel být platný, a nezálěží co na něj obránce zahlásil, jen musel fyzicky zasáhnout)"
                    ]



-- PASSIVE


viewPassive : DatabaseCache -> Pasivni -> List (Html msg)
viewPassive cache (Pasivni attrs sleva) =
    [ UI.Definition.viewSimpleBlock "Technika pasivně přidává" <|
        DatabaseCache.viewValues "+" cache attrs
    , case sleva of
        Just slevaPayload ->
            viewSleva cache slevaPayload

        Nothing ->
            Html.text ""
    ]


viewSleva : DatabaseCache -> SlevaPayload -> Html msg
viewSleva cache sleva =
    UI.Definition.viewSimpleBlock "Technika pasivně zlevňuje sesílací cenu"
        (viewSlevaNormal cache sleva)


viewSlevaNormal : DatabaseCache -> SlevaPayload -> List (Html msg)
viewSlevaNormal cache sleva =
    viewSlevaSelector cache sleva.selector
        :: DatabaseCache.viewValues "-" cache sleva.cena


viewSlevaSelector : DatabaseCache -> TechnikaSelector -> Html msg
viewSlevaSelector cache selector =
    case selector of
        SelectVsechnyTechniky ->
            Html.text "Pro všechny techniky"

        SelectStrom ids ->
            DatabaseCache.viewListBuilder "a" (GUID.setToList ids)
                |> DatabaseCache.withLabel "Pro techniky ve stromech"
                |> DatabaseCache.buildInline cache

        SelectTechnika ids ->
            DatabaseCache.viewListBuilder "a" (GUID.setToList ids)
                |> DatabaseCache.withLabel "Pro techniky"
                |> DatabaseCache.buildInline cache


viewSlevaShort : DatabaseCache -> Pasivni -> List (Html msg)
viewSlevaShort cache (Pasivni _ sleva) =
    case sleva of
        Just slevaPayload ->
            viewSlevaSelectorShort cache slevaPayload.selector
                :: DatabaseCache.viewValuesAlias "-" cache slevaPayload.cena

        Nothing ->
            []


viewSlevaSelectorShort : DatabaseCache -> TechnikaSelector -> Html msg
viewSlevaSelectorShort cache selector =
    case selector of
        SelectVsechnyTechniky ->
            Html.text "Všechny techniky"

        SelectStrom ids ->
            DatabaseCache.viewListBuilder "a" (GUID.setToList ids)
                |> DatabaseCache.buildInline cache

        SelectTechnika ids ->
            DatabaseCache.viewListBuilder "a" (GUID.setToList ids)
                |> DatabaseCache.buildInline cache
