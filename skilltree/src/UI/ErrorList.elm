module UI.ErrorList exposing (view)

import Html.Styled as Html exposing (Html)


view : List String -> Html msg
view errors =
    Html.div
        []
        [ Html.strong
            []
            [ Html.text "ERROR"
            ]
        , Html.ul
            []
            (List.map viewError errors)
        ]


viewError : String -> Html msg
viewError error =
    Html.li
        []
        [ Html.text error
        ]
