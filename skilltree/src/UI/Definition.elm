module UI.Definition exposing (indentBlock, listLabeled, listLabeledWithComment, multiply, stringLabeled, stringLabeledWithAlias, stringListLabeled, viewIndented, viewLabel, viewName, viewSimpleBlock, viewSimpleBlockInner, viewSimpleBlockShort)

import Css
import Html.Styled as Html exposing (Html)
import Html.Styled.Attributes as Attrs
import List.Extra
import UI
import Usecase.DarkTheme exposing (Msg)



--INLINE


viewLabel : String -> Html msg
viewLabel label =
    Html.span
        [ Attrs.css
            [ UI.textMinor
            ]
        ]
        [ Html.text (label ++ ": ")
        ]


viewName : String -> Html msg
viewName label =
    Html.strong
        []
        [ Html.text label
        ]


multiply : ( Html msg, Int ) -> Html msg
multiply ( name, count ) =
    let
        countText : String
        countText =
            if count > 1 then
                String.fromInt count ++ "x "

            else
                ""
    in
    Html.span
        []
        [ Html.text countText
        , name
        ]


addSpacesToSeparator : String -> String
addSpacesToSeparator sep =
    case String.trim sep of
        "," ->
            ", "

        _ ->
            " " ++ sep ++ " "


join : String -> List (Html msg) -> Html msg
join sep list =
    case List.Extra.unconsLast list of
        Just ( last, first :: body ) ->
            Html.span
                []
                (List.intersperse (UI.gray ", ") (first :: body)
                    ++ [ UI.gray (addSpacesToSeparator sep)
                       , last
                       ]
                )

        Just ( last, [] ) ->
            Html.span
                []
                [ last ]

        Nothing ->
            Html.text ""



--ONELINE


stringLabeled : String -> Maybe String -> Html msg
stringLabeled label value =
    stringLabeledWithAlias label value Nothing


stringLabeledWithAlias : String -> Maybe String -> Maybe String -> Html msg
stringLabeledWithAlias label value alias_ =
    case value of
        Just val ->
            Html.div
                []
                [ viewLabel label
                , viewName val
                , UI.gray <|
                    case alias_ of
                        Just als ->
                            " (" ++ als ++ ")"

                        Nothing ->
                            ""
                ]

        Nothing ->
            Html.text ""


stringListLabeled : String -> String -> List String -> Html msg
stringListLabeled sep label values =
    listLabeled sep label (List.map viewName values)


listLabeled : String -> String -> List (Html msg) -> Html msg
listLabeled sep label values =
    listLabeledWithComment sep label values Nothing


listLabeledWithComment : String -> String -> List (Html msg) -> Maybe String -> Html msg
listLabeledWithComment sep label values comment =
    if List.length values > 0 then
        Html.div
            []
            [ viewLabel label
                |> UI.whenHtml (label /= "")
            , join sep values
            , case comment of
                Just commentText ->
                    UI.minor (" (" ++ commentText ++ ")")

                Nothing ->
                    Html.text ""
            ]

    else
        Html.text ""



-- BLOCK


indentBlock : Css.Style
indentBlock =
    Css.paddingLeft (Css.rem 1)


viewIndented : Html msg -> Html msg
viewIndented content =
    Html.div
        [ Attrs.css [ indentBlock ]
        ]
        [ content
        ]


viewSimpleBlock : String -> List (Html msg) -> Html msg
viewSimpleBlock label items =
    viewSimpleBlockInner False label items


viewSimpleBlockShort : String -> List (Html msg) -> Html msg
viewSimpleBlockShort label items =
    viewSimpleBlockInner True label items


viewSimpleBlockInner : Bool -> String -> List (Html msg) -> Html msg
viewSimpleBlockInner short label items =
    if List.length items == 0 then
        Html.text ""

    else
        let
            gap : String
            gap =
                if short then
                    "0px"

                else
                    "3px"
        in
        Html.div
            [ Attrs.css
                [ Css.displayFlex
                , Css.flexDirection Css.column
                , Css.property "gap" gap
                , Css.alignItems Css.flexStart
                ]
            ]
            [ UI.little label
            , Html.div
                [ Attrs.css
                    [ Css.displayFlex
                    , Css.flexDirection Css.column
                    , indentBlock
                    , Css.property "gap" gap
                    , Css.alignItems Css.flexStart
                    ]
                ]
                items
            ]
