module UI.Heading exposing (Props, view)

import Css
import Html.Styled as Html exposing (Html)
import Html.Styled.Attributes as Attrs
import UI


type alias Props msg =
    { level : List (Html.Attribute msg) -> List (Html msg) -> Html msg
    , heading : String
    , alias : Maybe String
    , subtitle : Maybe String
    , image : Maybe String
    }


view : Props msg -> UI.StylableElement msg
view props style =
    Html.div
        [ Attrs.css
            [ Css.marginBottom (Css.rem 0.5)
            , Css.batch style
            ]
        ]
        [ case props.image of
            Just url ->
                Html.div
                    [ Attrs.css
                        [ Css.float Css.right
                        , Css.paddingLeft (Css.rem 1)
                        , Css.displayFlex
                        , Css.flexDirection Css.column
                        , Css.alignItems Css.end
                        ]
                    , Attrs.title <|
                        "Všechny techniky v tomto stromě, které vyžadují pečetě, "
                            ++ "musejí mít tuto pečeť jako poslední. "
                            ++ "Předchozí pečetě mohou být libovolné, ale musejí být různé."
                    ]
                    [ UI.minor "Poslední pečeť:"
                    , Html.img
                        [ Attrs.src url
                        , Attrs.css
                            [ Css.width (Css.rem 4)
                            , Css.height (Css.rem 4)
                            ]
                        ]
                        []
                    ]

            Nothing ->
                Html.text ""
        , props.level
            [ Attrs.css
                [ Css.marginTop (Css.rem 0.5)
                , Css.marginBottom (Css.rem 0)
                ]
            ]
            [ Html.text props.heading
            , case props.alias of
                Just alt ->
                    Html.span
                        [ Attrs.css
                            [ Css.fontWeight Css.normal
                            , Css.fontSize (Css.rem 1)
                            ]
                        ]
                        [ Html.text (" (" ++ alt ++ ")")
                        ]

                Nothing ->
                    Html.text ""
            ]
        , case props.subtitle of
            Just subtitle ->
                UI.minor subtitle

            Nothing ->
                Html.text ""
        , Html.div
            [ Attrs.css [ Css.property "clear" "both" ]]
            []
        ]
