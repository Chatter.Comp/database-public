module JutsuVylepseniTest exposing (..)

import Dict
import Expect exposing (Expectation)
import Fuzz exposing (Fuzzer, int, list, string)
import GUID
import JutsuEffectVylepseni
import Test exposing (..)
import Xml.Decode



-- DATA


testXml : String
testXml =
    """
    <root id="technika-root">
        <a>
            <b />
        </a>
        <target>
            <b>B1</b>
            <c>C1</c>
        </target>
        <target>
            <b>B2</b>
            <c>C2</c>
        </target>
        <target vylepšení="technika-xyz">
            <b>B3</b>
            <c>C3</c>
        </target>
        <target vylepšení="technika-xyz">
            <b>B4</b>
            <c>C4</c>
        </target>
        <a>
            <b />
        </a>
    </root>
    """


expectedPayload : Dict.Dict String (List TestPayload)
expectedPayload =
    Dict.empty
        |> Dict.insert "technika-root"
            [ { b = "B1"
              , c = "C1"
              }
            , { b = "B2"
              , c = "C2"
              }
            ]
        |> Dict.insert "technika-xyz"
            [ { b = "B3"
              , c = "C3"
              }
            , { b = "B4"
              , c = "C4"
              }
            ]



-- TEST


suite : Test
suite =
    describe "JutsuVylepseni"
        [ test "decoder" <|
            \_ ->
                testXml
                    |> Xml.Decode.run
                        (GUID.idDecoder
                            |> Xml.Decode.andThen
                                (\rootId ->
                                    JutsuEffectVylepseni.decoder [ "target" ] payloadDecoder rootId
                                )
                        )
                    |> Result.map (JutsuEffectVylepseni.expectEqual expectedPayload)
                    |> Result.mapError (Debug.log "Decoder error:")
                    |> Result.withDefault (Expect.fail "Decoder failed")
        ]



-- HELPERS


type alias TestPayload =
    { b : String
    , c : String
    }


payloadDecoder : Xml.Decode.Decoder TestPayload
payloadDecoder =
    Xml.Decode.map2 TestPayload
        (Xml.Decode.path [ "b" ] (Xml.Decode.single Xml.Decode.string))
        (Xml.Decode.path [ "c" ] (Xml.Decode.single Xml.Decode.string))
