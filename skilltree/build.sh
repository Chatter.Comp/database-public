cp src/Env/Env.elm src/Env/tmp-backup-Env.elm
cp src/Env/Util/BuildVariables.elm src/Env/Util/tmp-backup-BuildVariables.elm

echo "Building frontend..."

####################
# BUILD PARAMETERS #

# first param is the elm executable (path or command)
if [ "$1" = "" ]
then
    ELM="elm"
else
    ELM="$1"
fi
echo "  ELM: $ELM"

# second param can toggle production/staging build
if [ "$2" = "prod" ]
then
    FLAGS="--optimize"
    cat src/Env/Prod.elm > src/Env/Env.elm
    sed 's/Env.Prod/Env.Env/' src/Env/Env.elm > tmp_src_Env  # -i is incompatible on alpine
    cat tmp_src_Env > src/Env/Env.elm
    rm tmp_src_Env
elif [ "$2" = "stag" ]
then
    FLAGS="--optimize"
else
    FLAGS="--debug"
fi
echo "  FLAGS: $FLAGS"

# third param is version (tag or commit)
if [ "$3" ]
then
    VERSION=$(echo "$3" | tr / -)
else
    VERSION="dev-$(date +%s)"
fi
echo "  VERSION: $VERSION"

# fourth param is timestamp
if [ "$4" ]
then
    VERSION_DATE=$(echo "$4" | cut -dT -f1,1)
else
    VERSION_DATE=$(date -I)
fi
echo "  VERSION_DATE: $VERSION_DATE"

# 5th & 6th params are level of normal player and level of sensei
if [ "$5" -a "$6" ]
then
    LEVEL_NORMAL="$5"
    LEVEL_SENSEI="$6"
else
    LEVEL_NORMAL="5"
    LEVEL_SENSEI="6"
fi
echo "  LEVEL_NORMAL: $LEVEL_NORMAL"
echo "  LEVEL_SENSEI: $LEVEL_SENSEI"

echo "module Env.Util.BuildVariables exposing (..)" >  src/Env/Util/BuildVariables.elm

echo "version : String"                             >> src/Env/Util/BuildVariables.elm
echo "version = \"$VERSION\""                       >> src/Env/Util/BuildVariables.elm

echo "versionDate : String"                         >> src/Env/Util/BuildVariables.elm
echo "versionDate = \"$VERSION_DATE\""              >> src/Env/Util/BuildVariables.elm

echo "levelNormal : Int"                            >> src/Env/Util/BuildVariables.elm
echo "levelNormal = $LEVEL_NORMAL"                  >> src/Env/Util/BuildVariables.elm

echo "levelSensei : Int"                            >> src/Env/Util/BuildVariables.elm
echo "levelSensei = $LEVEL_SENSEI"                  >> src/Env/Util/BuildVariables.elm


#########
# BUILD #

FAILED="0"
for f in src/App/*.elm
do
    NAME=$(basename -s .elm "$f")
    NAME_LOWER=$(echo "$NAME" | tr '[:upper:]' '[:lower:]')
    echo ""
    echo "Building $NAME..."

    if [ $NAME_LOWER = 'skilltree' -o $NAME_LOWER = 'list' -o $NAME_LOWER = 'table' ]
    then
        TEMPLATE_FILE="app-index-template.html"
        if [ $NAME_LOWER = 'table' ]
        then
            TEMPLATE_FILE="app-index-table-template.html"
        fi
        echo "Generating $NAME_LOWER.html"
        sed "s/\\\$APP_NAME\\\$/$NAME/" "$TEMPLATE_FILE" | sed "s/\\\$APP_VERSION\\\$/$VERSION/" > "../public/$NAME_LOWER.html"
    fi

    if $ELM make $FLAGS "$f" --output "../public/bundle-$NAME.js"
    then
        echo "Build of $NAME: OK"
    else
        echo "Build of $NAME: ERROR"
        FAILED="1"
    fi
done


###########
# CLEANUP #

cat src/Env/tmp-backup-Env.elm > src/Env/Env.elm
rm src/Env/tmp-backup-Env.elm
cat src/Env/Util/tmp-backup-BuildVariables.elm > src/Env/Util/BuildVariables.elm
rm src/Env/Util/tmp-backup-BuildVariables.elm

if [ "$FAILED" = "1" ]
then
    exit 1
fi
